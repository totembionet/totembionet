package run;


import back.logic.Var;
import back.logic.blogic.*;
import back.logic.blogic.Comparator;
import util.jlist.OrderedArrayList;
import back.net.Gene;
import back.net.Net;
import back.net.Para;
import back.net.Reg;
import back.logic.Formula;
import util.Out;
import util.TotemBionetException;

import java.text.NumberFormat;
import java.util.*;


/**
 * Classe pour générer le graphe d'états sous la forme d'un fichier json
 * On peut générer un graphe synchrone et asynchrone
 *
 * Intentionnellement pas très objet ... pas de classe Graph et classes filles
 *     SyncGraph et AsyncGraph
 *
 * Des méthodes sur le réseau pourraient passer dans la classe Net mais pas utile
 * ailleurs
 *
 * @author helen
 */
class StateGraphGenerator {

    class ParametricEdge{
        long target;
        Formula constraint;

        ParametricEdge(long target,Formula constraint) {
            this.constraint=constraint;
            this.target = target;
        }

        public String toString() {
            return "-->" + target + "(const: " + constraint.toString() + ")";
        }

        public String toJSONString() {
//            return "{\"targ\": "+ target + ", \"cst\": " + constraintToJson(constraint) + "}";
            //TODO : ci-dessous : version avec String pour Benedetta mais la version dessous en long est meilleure
            final NumberFormat instance = NumberFormat.getNumberInstance();
            instance.setMinimumIntegerDigits(net.nbOfGene());
            instance.setGroupingUsed(false);
            return "{\"targ\": \""+ instance.format(target) + "\", \"cst\": " + constraintToJson(constraint) + "}";

        }

        // json de la contrainte sur les arcs
        private String constraintToJson(Formula constr) {
            if ((constr instanceof Great ) || (constr instanceof Less )|| (constr instanceof Equa )){
                Comparator cmp = (Comparator)constr;
                return cmp.toJsonString();
            }
            // cas du self loop
            if ((constr instanceof And))
                return constr.toJsonString();
            // Ne devrait pas se produire. si formule inconnue, renvoie null et BING!
            return null;
        }

    }
    private Net net;
    private HashMap<Long, HashSet<Long>> asyncGraph;
    private HashMap<Long, HashSet<Long>> syncGraph;
    private HashMap<Long, ArrayList<ParametricEdge>> parametricAsyncGraph;
    private boolean parametric;

    StateGraphGenerator(Net n) throws TotemBionetException {
        this(n,false);
    }

    StateGraphGenerator(Net n,boolean parametric) throws TotemBionetException {
        net=n;
        asyncGraph = new HashMap<Long, HashSet<Long>>();
        syncGraph = new HashMap<Long, HashSet<Long>>();
        parametricAsyncGraph = new HashMap<Long, ArrayList<ParametricEdge>>();
        this.parametric=parametric;
        buildGraphs();
    }

    private void buildGraphs() throws TotemBionetException{
        if (parametric)
            buildParametricAsyncGraph(parametricAsyncGraph);
        else {
            buildAsynchronousGraph(asyncGraph);
            buildSynchronousGraph(syncGraph);
        }
    }

    ///////////////////////////////////
    // gestion des arcs et construction du graphe

    /**
     *
     * @param from: départ de l'arc
     * @param to : arrivée de l'arc
     */
    private void addOneEdge(HashMap<Long, HashSet<Long>> g,Long from, Long to){
        if (g.containsKey(from)) {
            HashSet list = g.get(from);
            list.add(to);
        }
        else {
            HashSet list = new HashSet<Long>();
            list.add(to);
            g.put(from,list);
        }
    }

    /**
     * for adding an edge from vectors of Longs
     * @param from
     * @param to
     */
    private void addOneVectorEdge(HashMap<Long, HashSet<Long>> g,ArrayList<Integer> from, ArrayList<Integer> to){
        addOneEdge(g,vectorToLong(from),vectorToLong(to));
    }

    /**
     * construit le graphe synchrone
     * il faut énumérer tous les états possibles et dans chaque état
     * calculer le paramètre applicable => c'est le prochaine valeur de la variable
     * @param g : le graphe construit
     */
    public void buildSynchronousGraph(HashMap<Long, HashSet<Long>> g) throws TotemBionetException{
        //System.out.println("ordre des variables " + net.geneList());
        firstState();
        do {
//            System.out.println(net.currentState());
//            System.out.println(vectorToLong(net.currentState()));
//            System.out.println(getApplicableParas());
//            System.out.println(getApplicableParaValues());
            addOneVectorEdge(g,net.currentState(),getApplicableParaValues());
        } while (nextState());
    }

    /**
     * construit le graphe asynchrone
     * il faut énumérer tous les états possibles et dans chaque état
     * calculer le paramètre applicable
     * ensuite il faut asynchroniser en avançant par pas de 1
     * @param g : le graphe construit
     */
    public void buildAsynchronousGraph(HashMap<Long, HashSet<Long>> g) throws TotemBionetException {
        //System.out.println("ordre des variables " + net.geneList());
        firstState();
        do {
//            System.out.println(net.currentState());
//            System.out.println(vectorToLong(net.currentState()));
//            System.out.println(getApplicableParas());
//            System.out.println(getApplicableParaValues());
            addAsynchronousEdges(g,net.currentState(), getApplicableParaValues());
        } while (nextState());
    }

        /**
     * construit le graphe asynchrone parametrique
     * il faut énumérer tous les états possibles et dans chaque état
     * calculer le paramètre applicable, savoir s'il est connu dans le bloc PARA
         * et si ce n'est pas le cas, créer un arc étiqueté
     *
     * @param g : le graphe construit
     */
    public void buildParametricAsyncGraph(HashMap<Long, ArrayList<ParametricEdge>> g) throws TotemBionetException {
        //System.out.println("ordre des variables " + net.geneList());
        firstState();
        do {
//            System.out.println(net.currentState());
//            System.out.println(vectorToLong(net.currentState()));
//            System.out.println(getApplicableParas());
//            System.out.println(getApplicableParaValues());
            addParametricAsyncEdges(g,net.currentState(), getApplicableParas());
        } while (nextState());
    }

        /**
     * Construit les arcs sortants pour un état donné
         * NOTA : tout repose sur le fait que l'état, les gènes et les paramètres applicables
         * sont dans le même ordre dans les listes
         *
     * @param g
     * @param state : état courant
     * @param appParas : liste des paramètres applicables pour l'état courant state
     */
    private void addParametricAsyncEdges( HashMap<Long, ArrayList<ParametricEdge>> g, ArrayList<Integer> state, ArrayList<Para> appParas) {
        OrderedArrayList<Gene> genes = net.genes();
        ArrayList<ParametricEdge> edges = new ArrayList<>();
        Formula selfConstraint = null;
        for (int i = 0; i < state.size(); i++) {
            int minVal =genes.get(i).min();
            int maxVal =genes.get(i).max();
            int valI = state.get(i);
            Int valIFormula = new Int(valI);
            Para appPara = appParas.get(i);
            Var appParaVar = new Var(appPara.getSMBname());
            //increase
            if (valI+1<=maxVal){
                ArrayList<Integer> nstIncrease = (ArrayList<Integer>) state.clone();
                nstIncrease.set(i,valI+1);
                long nextState = vectorToLong(nstIncrease);
                if (!appPara.fixe) {
                    edges.add(new ParametricEdge(nextState,new Great(appParaVar,valIFormula)));
                    // TODO: si la valeur du para fixe est contradictoire exception ou pas d'arc ?
                }
                // is applicable parameter is fixed and greater than current value,
                // the edge exists with no condition (thus condition True)
                else {
                    if (appPara.currentMin()>valI)
                        edges.add(new ParametricEdge(nextState, new True()));
                    // else the edge can not be taken => it is not added
                }
            }
            // decrease
            if (valI-1>=minVal){
                ArrayList<Integer> nstDecrease = (ArrayList<Integer>) state.clone();
                nstDecrease.set(i,valI-1);
                long nextState =vectorToLong(nstDecrease);
                if (!appPara.fixe) {
                    edges.add(new ParametricEdge(nextState,new Less(appParaVar,valIFormula)));
                }
                else {
                    if (appPara.currentMin() < valI){
                        edges.add(new ParametricEdge(nextState, new True()));
                    }
                }
            }
            // stable
            Formula cst = (appPara.fixe)?new True():new Equa(appParaVar,valIFormula);
            if (selfConstraint==null)
                selfConstraint = cst;
            else
                selfConstraint = new And(selfConstraint,cst);
        }
        // self-loop
        edges.add(new ParametricEdge(vectorToLong(state),selfConstraint));
        g.put(vectorToLong(state),edges);
    }


    /**
     *
     * @param g
     * @param state : état courant
     * @param nextState : valeurs des paramètres applicables pour l'état suivant
     */
    private void addAsynchronousEdges(HashMap<Long, HashSet<Long>> g, ArrayList<Integer> state, ArrayList<Integer> nextState) {
        for (int i = 0; i < state.size(); i++) {
            //System.out.println("couple " + state + "=>" + nextState);
            int sti = state.get(i);
            int nsti = nextState.get(i);
            ArrayList<Integer> nst = (ArrayList<Integer>) state.clone();
            if (sti < nsti) {
                nst.set(i, sti+1);
                //System.out.println("adding edge " + state + "=>" + nst);
                addOneVectorEdge(g,state, nst);
            }
            if (sti > nsti) {
                nst.set(i, sti-1);
                //System.out.println("adding edge " + state + "=>" + nst);
                addOneVectorEdge(g,state, nst);
            }
        }
    }
    ///////////////////////////////////
    // méthodes nécessaires sur le réseau net

    /**
     * pour parcourir tous les états
     */
    private void firstState() {
        for (Gene g : net.getGenes()) {
            if (!net.isEnvVar(g))
                g.firstLevel();
        }
    }
    private boolean nextState() {
        for (Gene g : net.getGenes()) {
            if (!net.isEnvVar(g) & g.nextLevel())
                return true;
        }
        return false;
    }

    /**
     *
     * @return les paramètres applicables sur l'état courant
     * @throws TotemBionetException
     */
    private ArrayList<Para> getApplicableParas() throws TotemBionetException{
        ArrayList<Para> app = new ArrayList<Para>();
        for (Gene g : net.getGenes()) {
            if (!net.isEnvVar(g))
                app.add(getApplicablePara(g));
        }
        return app;
    }

        /**
     *
     * @return la valeur des paramètres applicables sur l'état courant
     * @throws TotemBionetException
     */
    private ArrayList<Integer> getApplicableParaValues() throws TotemBionetException {
        ArrayList<Integer> app = new ArrayList<Integer>();
        for (Gene g : net.getGenes()) {
            if (!net.isEnvVar(g))
                // NOTA: je prends le min, puisque le paramètre est fixe,
                // ça ne pose pas de problème
                app.add(getApplicablePara(g).currentMin());
        }
        return app;
    }

    /**
     *
     * @param g : un gène
     * @return : le paramètre applicable de ce gène pour la
     *           configuration actuelle de l'état
     */
    private Para getApplicablePara(Gene g) throws TotemBionetException{
        OrderedArrayList<Reg> paraRegs = new OrderedArrayList<Reg>();
        for (Reg r : g.getRegs()) {
            if (r.isEffective()) {
                paraRegs.addAlpha(r);
            }
        }
        //On construit le nom du param�tre
        String paraNuSMVName = "K" + g.name;
        for (int i = 0; i < paraRegs.size(); i++) {
            paraNuSMVName += "_" + paraRegs.get(i);
        }
        //On regarde si le param�tre existe d�j�
        Para p = g.getParas().getWithStringId(paraNuSMVName);
        if (p.fixe | parametric)
            return p;
        throw new TotemBionetException("All parameters must have been set " +
                "before building the state transition graph.\n" +
                "Use option \"-paras\" to know the set of effective parameters");
    }

    /**
     * utilitaire pour traduire un vecteur d'entiers en Long
     */
    private Long vectorToLong(ArrayList<Integer> list){
        long r =0;
        for (Integer i : list) {
            r = r*10 + i;
        }
        return r;
    }

    //////////////////////////////////////
    // affichages
    // dont print the graph but only info on it (async graph may be very large)
    public String toString() {
        return "Asynchronous graph:\n" + toString(asyncGraph) + "\n" + "Synchronous graph:\n" + toString(syncGraph) +"\n";
    }

    private String toString(HashMap<Long,HashSet<Long>> map) {
        int nbEdges=0;
        for (Map.Entry<Long, HashSet<Long>> h: map.entrySet()){
            nbEdges+=h.getValue().size();
        }
        String res= "   #vertices: " + map.size() + ", #edges: " + nbEdges;
        if (Out.verb()>=1)
            return res + "\n" + map;
        return res;
    }


    // format json pour les listes d'adjacence
    private String adjListToJson(HashMap<Long, HashSet<Long>> g){
        String j= "[";
        int i=0;
        for (Map.Entry<Long, HashSet<Long>> h: g.entrySet()) {
            j += "{" ;
            j += "\"node\": " + h.getKey() + ",";
            j += "\"succs\": " + h.getValue() ;
            j +="}";
            if (i<g.size()-1)
                j+= ",";
            i++;
        }
        j+= "]";
        return j;
    }

    public String toJson() {
        if (parametric)
            return toJson(parametricAsyncGraph);
        else {
            System.out.println("Generating synchronous and asynchronous state transition graph ...");
            System.out.println(this);
            String j = "{ \"sync-graph\":";
            j += adjListToJson(syncGraph);
            j += ", \"async-graph\":";
            j += adjListToJson(asyncGraph);
            j += "}";
            return j;
        }
    }

    // json for the parametric asynchronous graph
    private String toJson(HashMap<Long, ArrayList<ParametricEdge>> paramGraph) {
        String j = "{ \"PSTG\":";
        // number of vertices
        j += "{\"ver-num\":" + paramGraph.size() + ",";
        j += "\"gene-list\":" + toJsonList(net.genes()) + ",";
        // graph
        j += "\"graph\": [";
        int i=0;
        // adjency list
        for (Map.Entry<Long,ArrayList<ParametricEdge>>  h: paramGraph.entrySet()) {
            // formating the state according to the number of variables
            final NumberFormat instance = NumberFormat.getNumberInstance();
            instance.setMinimumIntegerDigits(net.nbOfGene());
            instance.setGroupingUsed(false);
            j += "{" ;
            j += "\"ver\": \"" + instance.format(h.getKey()) + "\",";
            j += "\"edg\": [" + edgesToJson(h.getValue())  + "]";
            j +="}";
            if (i<paramGraph.size()-1)
                j+= ",";
            i++;
        }
        j+= "],";
        // temporal logic formula = CTL + FAIRCTL blocks
        // TODO: faire la transformation pour utiliser les opérateurs EX, EF et EU uniquement
        List<Formula> ctlbk = buildCTL(net.getCtl(),net.getFairCtl());
        j+="\"ctlbk\":" + makeJsonAnd(ctlbk) ;
        j += "}}";
        return j;
    }

    // to tranform the list of gene variables as a json list
    private String toJsonList(OrderedArrayList<Gene> vars) {
        String res = "[";
        for (Gene g : vars) {
            if (!net.isEnvVar(g)) {
                res += "{\"g\":\"" + g + "\",";
                res += "\"d\":[" + g.min() + "," + g.max() + "]},";
            }
        }
        res=res.substring(0,res.length()-1);
        res +="]";
        return res;
    }

    // to make a unique list of ctl formulas from CTL block ant FAIRCTL block
    private List<Formula> buildCTL(List<Formula> ctl, List<Formula> fctl) {
        List<Formula> ctls = new ArrayList<Formula>();
        for (Formula f : ctl)
            ctls.add(f);
       for (Formula f : fctl)
            ctls.add(f.fairBio());
        return ctls;
    }

    // to make the conjunction of the list of CTL formulas
    private String makeJsonAnd(List<Formula> ctl) {
        if (ctl.size()==1)
            return ctl.get(0).toJsonString();
        return makeJsonAnd(ctl,0,ctl.size());
    }

    // to make the conjunction of the list of CTL formulas => recursive one
    private String makeJsonAnd(List<Formula> ctl,int i, int size) {
        if (i==size-1)
            return ctl.get(i).toJsonString() ;
        Formula ctlf = ctl.get(i);
        String res= "{\"op\":" + "\"AND\",";
        res += "\"left\":" + ctlf.toJsonString() + ",\"right\":" + makeJsonAnd(ctl, i+1,size) + "}";
        return res;
    }

    // the edges
    private String edgesToJson(ArrayList<ParametricEdge> edges){
        String res = "";
        int i=0;
        for (ParametricEdge edg : edges){
            res += edg.toJSONString();
            if (i<edges.size()-1)
                res+= ",";
            i++;
        }
        return res;
    }

}
