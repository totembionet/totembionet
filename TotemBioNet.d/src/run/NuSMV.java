package run;

import back.logic.blogic.And;
import back.logic.blogic.Great;
import back.logic.blogic.Int;
import back.logic.blogic.Less;
import util.jlist.OrderedArrayList;
import back.search.*;
import util.jclock.Clock;
import back.logic.Formula;
import back.logic.Var;
import back.net.Gene;
import back.net.Net;
import back.net.Para;

import java.io.*;
import java.util.List;
import java.util.ArrayList;

/**
 * @author Adrien Richard
 * @author Hélène Collavizza
 * <psf>
 * La construction du fichier smv a été complètement modifiée.
 * La structure de Kripke est paramétrée par les K qui sont des variables de type
 * FROZEN_VAR
 * Les valeurs des K sont des tableaux de byte
 */

public class NuSMV {

    // le chemin vers NuSMV en dur
    public static String NUSMVPATH;

    public static void setNuSMVPath() {
        String env = System.getenv("NUSMVPATH");
        NUSMVPATH = (env != null) ? env : "lib/NuSMV/linux/NuSMV-2.6.0-Linux/bin/NuSMV";
    }

    private static void writeHeader(BufferedWriter w, String name, boolean formal, boolean csv) throws Exception {
        w.write("-- NuSMV file written by TotemBioNet V2.3.1\n");
        if (formal)
            w.write("-- The model is partly instanciated\n");
        if (csv)
            w.write("-- Models are written in CSV file\n");
        w.write("\n\nMODULE main\n\n");
    }


    public static void writeNet(Net net, String name, byte[] param, boolean csv, boolean fromFile) throws Exception {
        boolean formal = (param == null) ? false : param[0] == -1;
        BufferedWriter w = new BufferedWriter(new FileWriter(new File(name + ".smv")));
        writeHeader(w, name, formal, csv);

        // éciture des différentes parties du fichier .smv
        writeVars(net, w);
        if (formal)
            writeParamVars(net, w, param);
        else if (fromFile)
            writeParamFromMDD(net, w, param);
        else
            writeParamVars(net, w);
        writeEnvVars(net, w);
        writeInit(net,w);
        writeDefine(w);
        writeAssign(w);
        if (formal)
            writeSnoussiConstraints(net, w);
        writeTrans(net, w);
        writeSpec(net, w, formal);
        writeFairSpec(net, w, formal);
        w.close();
    }

    //VAR
    private static void writeVars(Net net, BufferedWriter w) throws Exception {
        w.write("VAR\n\n");
        // gènes
        w.write("-- genes\n");
        for (Gene g : net.genes()) {
            if (!net.isEnvVar(g))
                w.write(g + " : " + g.min() + " .. " + g.max() + " ;\n");
        }
    }

    // VARIABLES PARAMETRES
    // ce sont des frozenvars : ne changent pas d'état
    // les paramètres fixés dans paramValue ont pour domaine de définition x..x où x
    // est leur valeur, sinon, ils ont pour domaine le domaine du gène correspondant
    private static void writeParamVars(Net net, BufferedWriter w, byte[] paramVal) throws Exception {
        // domaine déclaration des paramètres
        // parameters
        w.write("\nFROZENVAR\n");
        w.write("\n-- parameters\n");
        System.out.println("write param " + paramVal);
        for (int i = 0; i < net.genes().size(); i++) {
            Gene g = net.genes().get(i);
            int[] index = ParasAsArray.debFin(i);
            //System.out.println(g.dagByDeep);
            //System.out.println("-- K" + g + ": " + ParasAsArray.getParaValue(paramVal, index[0], index[1] + 1));
            //System.out.println("\n-- K" + g + ": " + ParasAsArray.getParaValue(paramVal, index[0], index[1] + 1) + "\n");
            w.write("\n-- K" + g + ": " + ParasAsArray.getParaValue(paramVal, index[0], index[1] + 1) + "\n");
            int ind = index[1];
            List<Para> pars = g.getParas();
            for (int j = pars.size() - 1; j >= 0; j--) {
                Para p = pars.get(j);
                if (paramVal[ind] != -1) {
                    //System.out.println(psf.getNuSMVname() + " : " + paramVal[ind] + " .. " + paramVal[ind] + ";\n");
                    w.write(p.getNuSMVname() + " : " + paramVal[ind] + " .. " + paramVal[ind] + ";\n");
                } else {
                    // cas formel
                    if (!g.selfRegulated()) {
                        //System.out.println(psf + ": " + psf.currentMin() + " .. " + psf.maxLevel());
                        w.write(p.getNuSMVname() + " : " + p.currentMin() + " .. " + p.maxLevel() + " ; --free param\n");
                    } else {
                        //System.out.println("SELF " + psf.getNusSMVname() + " : " + paraValuesSet(psf, psf.currentMin()) + " ; --free param\n");
                        w.write(p.getNuSMVname() + " : " + paraValuesSet(p, p.currentMin()) + " ; --free param\n");
                    }
                }
                ind--;
            }
        }
    }


    // pour avoir toutes les valeurs y compris des variables auto-régulées
    private static String paraValuesSet(Para p, int min) {
        String v = "{";
        ArrayList<Integer> a = p.valueSet();
        int s = a.size();
        int i = 0;
        while (i < s && min > a.get(i))
            i++;
        if (i == s - 1)
            return "{" + a.get(i) + "}";
        while (i < s - 1) {
            v += a.get(i) + ",";
            i++;
        }
        v += a.get(i) + "}";
        return v;
    }

    // VARIABLES PARAMETRES
    // ce sont des frozenvars : ne changent pas d'état
    // les paramètres fixés dans paramValue ont pour domaine de définition x..x où x
    // est leur valeur, sinon, ils ont pour domaine le domaine du gène correspondant
    private static void writeParamVars(Net net, BufferedWriter w) throws Exception {
        // domaine déclaration des paramètres
        // parameters
        w.write("\nFROZENVAR\n");
        w.write("\n-- parameters\n");
        //System.out.println("write param");
        for (int i = 0; i < net.genes().size(); i++) {
            Gene g = net.genes().get(i);
            //int[] index = ParasAsArray.debFin(i);
            //System.out.println(g.dagByDeep);
            //System.out.println("-- K" + g + ": " + ParasAsArray.getParaValue(paramVal, index[0], index[1] + 1));
            //int min = g.min;
            //int max = g.max;
            List<Para> pars = g.getParas();
            for (int j = pars.size() - 1; j >= 0; j--) {
                Para p = pars.get(j);
                w.write(p.getNuSMVname() + " : " + p.currentMin() + " .. " + p.currentMin() + " ; \n");
            }
        }
    }

    private static void writeParamFromMDD(Net net, BufferedWriter w, byte[] model) throws Exception {
        // domaine déclaration des paramètres
        // parameters
        w.write("\nFROZENVAR\n");
        w.write("\n-- parameters\n");
        //System.out.println("write param");
        for (int i = 0; i < net.genes().size(); i++) {
            Gene g = net.genes().get(i);
            int[] index = ParasAsArray.debFin(i);
            //System.out.println(g.dagByDeep);
            //System.out.println("-- K" + g + ": " + ParasAsArray.getParaValue(paramVal, index[0], index[1] + 1));
            //int min = g.min;
            //int max = g.max;
            int ind = index[1];
            List<Para> pars = g.getParas();
            for (int j = pars.size() - 1; j >= 0; j--) {
                Para p = pars.get(j);
                byte val = model[ind];
                //.out.println("nusmv para : " + val);
                if (val == -1)
                    w.write(p.getNuSMVname() + " : " + p.currentMin() + " .. " + p.maxLevel() + " ; --any param\n");
                else
                    w.write(p.getNuSMVname() + " : " + val + " .. " + val + " ; \n");
                ind--;
            }
        }
    }


    private static void writeEnvVars(Net net, BufferedWriter w) throws Exception {
        if (net.getEnvVar().size() != 0) {
            w.write("\nFROZENVAR\n");
            w.write("\n-- environnement variables\n");
            w.write(StaticInformation.ENV_VAR);
        }
    }

    //DEFINE
    private static void writeDefine(BufferedWriter w) throws Exception {
        w.write("\nDEFINE\n\n");
        //Définition, pour chaque gène g, pour tous les modèles, d
        // des fonctions qui donnent le
        //paramètre vers lequel g évolue en fonction de l'état du
        //réseau et du modèle choisi
        w.write("-- fonction focale\n\n");
        for (String f : StaticInformation.FOCAL_VALUES)
            w.write(f);
    }

    private static void writeAssign(BufferedWriter w) throws Exception {
        //ASSIGN
        w.write("ASSIGN\n\n");
        w.write(StaticInformation.ASSIGN);
    }

    private static void writeInit(Net net,BufferedWriter w) throws Exception {
       if (net.hasInitBlock()) { //INIT block
           w.write("\n-- Initial conditions\n");
           w.write(StaticInformation.INIT);
       }
    }

    //TRANS
    private static void writeTrans(Net net, BufferedWriter w) throws Exception {
        w.write("\nTRANS\n");
        w.write(StaticInformation.TRANS);
    }

    //SPEC
    private static void writeSpec(Net net, BufferedWriter w, boolean isPattern) throws Exception {

        List<Formula> ctl = net.getCtl();
        if (ctl.size() == 0 && net.getFairCtl().size() == 0) {
            w.write("\n\nCTLSPEC\n");
            w.write("TRUE\n\n");
        } else {
            w.write("\n\n--SPEC : CTL part");
            if (!isPattern)
                w.write(StaticInformation.PHI);
            else {
                w.write(StaticInformation.NOT_PHI);
            }
        }
    }

    //SPEC
    private static void writeFairSpec(Net net, BufferedWriter w, boolean isPattern) throws Exception {
        List<Formula> c = net.getFairCtl();
        if (c.size() != 0) {
            w.write("\n\n--SPEC : FAIR CTL part");
            if (!isPattern)
                w.write(StaticInformation.FAIR_PHI);
            else {
                w.write(StaticInformation.FAIR_NOT_PHI);
            }
        }
    }


    // pour les contraintes de Snoussi sur les paramètres formels
    private static void writeSnoussiConstraints(Net net, BufferedWriter w) throws Exception {
        w.write("\n-- monotonicity constraints on formal parameters\n\n");
        for (Gene g : net.genes()) {
            if (g.dagSize() > 2) {
                List<Para> p = g.getParas();
                for (Para pi : p) {
                    // on ne met les contraintes que pour les paramètres
                    String invar = "INVAR ";
                    boolean first = false;
                    // on met les contraintes de Snoussi sur la partie libre
                    // et sur la dernière relation pi <= pi.suc où pi.suc est non libre
                    if (pi.libre && pi.succs.size() != 0) {
                        ArrayList<Para> successors = pi.succs;
                        for (Para sp : successors) {
                            if (sp.libre) {
                                //String paraVal = (sp.libre) ? sp.toNuSMVString() : sp.currentMin() + "";
                                String paraVal = sp.getNuSMVname();
                                if (!first) {
                                    invar += pi.getNuSMVname() + " <= " + paraVal;
                                    first = true;
                                } else
                                    invar += " & " + pi.getNuSMVname() + " <= " + paraVal;
                            }
                        }
                        /*
                        if (j < successors.size() && !successors.getWithStringId(j).libre) {
                            if (!first) {
                                invar += pi + " <= " + successors.getWithStringId(j).currentMin();
                                first = true;
                            } else
                                invar += " & " + pi + " <= " + successors.getWithStringId(j).currentMin();
                        }*/
                    }
                    if (first)
                        w.write(invar + ";\n");
                }
            }
        }
        w.write("\n");
    }

    public static void run(Net net, String fileName, boolean dynamics, Run.SearchType type, boolean csv) throws Exception {

        // gère l'option de réordonnement dynamique des variables
        String nuSMVcommand = (dynamics) ? NUSMVPATH + " -coi -dcx -df -dynamic " : NUSMVPATH + " -coi -dcx -df ";
        // sans spec, on ne fait pas de formel, on coupera rien
        if (StaticInformation.isEmptySpec(net))
            type = Run.SearchType.FORMAL;
        // search permet de gérer les pattern / moèles
        Search search;
        switch (type) {
            // on suppose que le formel ne traite pas le filtrage de Hoare dynamique
            case FORMAL:
                search = new FormalSearchEngine(net, nuSMVcommand);
                break;
            case HOARE:
                if (net.needDynamicHoare()) {
                    search = new InstanciatedSearchEngineWithHoare(net, nuSMVcommand);
                }
                else {
                    search = new InstanciatedSearchEngine(net, nuSMVcommand);
                }
                break;
            case FROM_PARA_SET:
                search = new FromFileSearchEngine(net, nuSMVcommand,Main.fromFile(),Main.fromType());
                break;
            case FROM_PARA_HOARE:
                search = new FromFileSearchEngineWithHoare(net, nuSMVcommand,Main.fromFile(),Main.fromType());
                break;

            default:
                search = new InstanciatedSearchEngine(net, nuSMVcommand);
        }
        // calcule toutes les informations sur le réseau qui ne dépendent pas des
        // valeurs du modèle
        StaticInformation.computeStaticInformation(net);

        Clock total = new Clock();

        // boucle de recherche de tous les modèles
        search.initSearch(fileName);
        int k = 0;
        do {
            search.doSearch(fileName, csv);
            k++;
        } while (search.hasNext());//&&k<20);

        if (!csv)
            search.writeSearch(total);
        else
            search.writeSearchInCSV(total, fileName);
    }


    public static void runDebug(Net net, String fileName,  String from) throws Exception {
        System.out.println("*****************");
        System.out.println("***** DEBUG *****");
        System.out.println("*****************");

//        ParametricModelChecker pm = new ParametricModelChecker("private-examples/modelCheck/",net);
//        pm.modelCheck();
//        System.out.println(pm);
//
//        //pm.solve();
//        System.out.println("\n*****************");
        //System.out.println(pm);

        Formula f = new And(new Great(new Var("KMucus_prod"),new Int(0)),
                new Less(new Var("KOperon_alg_free"),new Int(1)));
        System.out.println(f);
        System.out.println(f.toCNF());

//        YicesLauncher yl = new YicesLauncher("examples/modelCheck/",net);
//        yl.writeYices(new And(new Great(new Var("KMucus_prod"),new Int(0)),
//                new Less(new Var("KOperon_alg_free"),new Int(1))));
//        System.out.println(yl.executeYicesCommand());

//        System.out.println(pm.possibleNextStates("00"));
//        System.out.println(pm.possibleNextStates("10"));

//        System.out.println(net.genes());
//        FromFileParaEnumerator fpe = new FromFileParaEnumerator("OK",from,net.genes());
//        System.out.println(fpe);
//        fpe.firstParameterization();
//        System.out.println(Arrays.toString(fpe.getCurrentModel()));
//        while (fpe.nextParameterization())
//            System.out.println(Arrays.toString(fpe.getCurrentModel()));
//        fpe.close();
//        int[] pat = {0, 0 ,0 ,0, 0, 0};
//        MDDBuilder mddB = new MDDBuilder();
//        MDDParaSet paras = mddB.createFromOneRow("okModels",pat,net.genes(),"OK");
//        System.out.println(paras);
//        int[] pat2 = {0, 0 ,1 ,0, 1, 0};
//        paras.addRow(pat2);
//        System.out.println(paras);

//        FromFileSearchEngine search = new FromFileSearchEngine(net,"NuSMV","/home/helen/BIO-recherche/private-totem/totembionet/examples/mucusOperon/mucusOperon-save.csv","OK");
//        StaticInformation.computeStaticInformation(net);
//
//        byte[] pat = {-1, 0 ,0 ,0, 2, -1, 1};
//        System.out.println("pat " + ParasAsArray.getCSV(3,pat,true));
//
//
//        Clock total = new Clock();
//
//        // boucle de recherche de tous les modèles
//        search.initSearch(fileName);
//        int k = 0;
//        do {
//            search.doSearch(fileName, true);
//            k++;
//        } while (search.hasNext());//&&k<20);
//
//
//            search.writeSearchInCSV(total, fileName);
//        StateGraphGenerator stg = new StateGraphGenerator(net);
//        //System.out.println(stg);
//        System.out.println("json : " + stg.toJson());
//        net.printForbiddenParas();
//        net.printParameterSet();
//        net.print();
//        NuSMV.setNuSMVPath();
    }


        /**
         * this class allows to store the information which is common
         * to every pattern and model
         *
         * @author Hélène Collavizza
         */
    private static class StaticInformation {
        private static String PHI; // formule CTL
        private static String NOT_PHI;  // négation de la formule CTL
        private static String FAIR_PHI; // formule CTL fair
        private static String FAIR_NOT_PHI;  // négation de la formule CTL faire
        private static String ENV_VAR; // bloc des variables d'environnement
        private static ArrayList<String> FOCAL_VALUES; // fonctions focales
        private static String ASSIGN;  // bloc ASSIGN qui définit la valeur que peut prendre le gène en fonction de sa fonction focale
        private static String TRANS;  // bloc TRANS qui indique d'une seule transition est possible
        private static String INIT="";  // bloc TRANS qui indique d'une seule transition est possible

        private static void computeStaticInformation(Net net) throws Exception {
            computeFocalValue(net);
            computeTrans(net);
            computeAssign(net);
            computeNotPhi(net.getCtl());
            computePhi(net.getCtl());
            computeNotFairPhi(net.getFairCtl());
            computeFairPhi(net.getFairCtl());
            computeEnvVar(net.getEnvVar());
            computeInit(net.getInit());
        }

        // pour savoir s'il n'y a pas de spec
        public static boolean isEmptySpec(Net net) {
            return net.getCtl().isEmpty() && net.getFairCtl().isEmpty();
        }

        // créer une String contenant la définition de F_g(K) pour chaque gène
        // FOCAL_VALUES.getWithStringId(i) est la fonction focale du gène i
        private static void computeFocalValue(Net net) throws Exception {
            FOCAL_VALUES = new ArrayList<String>();
            for (Gene g  : net.genes()) {
                if (!net.isEnvVar(g)) {
                    // focal value for gene i
                    //Cette fonction s'appelle F_g_i où i est le n° du modèle
                    String fi = "F_" + g + " :=";
                    //Cas particulier ou le gène à 1 paramètre
                    if (g.paraSize() == 1) {
                        //On écrit juste la valeur de ce paramètre (valeur
                        //quelconque prise dans l'intervalle courant)
                        fi += g.getPara(0).getNuSMVname() + ";\n\n";
                    }
                    //Cas général
                    else {
                        fi += "\ncase\n";
                        //Pour chaque paramètre
                        for (int j = g.paraSize() - 1; j >= 0; j--) {
                            Para p = g.getPara(j);
                            //on écrit les formules régulations associées à psf
                            if (p.regs.isEmpty())
                                //Cas particulier où il n'y a pas de formule
                                fi += "TRUE";
                            else
                                //Cas général
                                for (int k = 0; k < p.regs.size(); k++) {
                                    fi += p.getFormula(k).toString();
                                    if (k < p.regs.size() - 1)
                                        fi += " & ";
                                }
                            // on écrit le nom du paramètre pour ce cas
                            fi += " : " + p.getNuSMVname() + " ; \n";
                        }
                        fi += "esac;\n\n";
                    }
                    FOCAL_VALUES.add(fi);
                }
            }
        }

        // pour indiquer qu'une seule transition n'est possible à la fois
        private static void computeTrans(Net net) throws Exception {
            String res = "(";
            //Stabilité
            int nbGene = 0;
            for (Gene g : net.genes()) {
                if (!net.isEnvVar(g)) {
                    res += g + " = F_" + g;
                    if (nbGene < net.nbOfGene() - 1)
                        res += " & ";
                    nbGene++;
                }
            }
            res += ") |\n";
            nbGene=0;

            //Ou évolution du ième gène uniquement
            OrderedArrayList ng = net.genes();
            for (int i = 0; i < ng.size(); i++) {
                if (!net.isEnvVar((Gene)ng.get(i))) {
                    res += "(";
                    int nbGeneJ = 0;
                    for (int j = 0; j < ng.size() ; j++) {
                        if (!net.isEnvVar((Gene)ng.get(j))) {
                            if (i == j)
                                res += ng.get(j) + " != next(" + ng.get(j) + ")";
                            else
                                res += ng.get(j) + "  = next(" + ng.get(j) + ")";
                            if (nbGeneJ < net.nbOfGene() - 1)
                                res += " & ";
                            nbGeneJ++;
                        }
                    }
                    res += ")";
                    if (nbGene < net.nbOfGene() - 1)
                        res += " |\n";
                    nbGene++;
                }
            }
            TRANS = res;
        }

        //Directions d'évolution des gènes
        // si égal à F vaut F, si <F on peut aller vers F-1 ou F sinon vers F+1 ou F
        private static void computeAssign(Net net) throws Exception {
            String res = "";
            for (Gene g : net.genes()) {
                if (!net.isEnvVar(g)) {

                    //Pour la version NuSMV 2.4 qui fait la différence entre
                    //le type booleén et entier
                    if (g.max() - g.min() == 1)
                        res += "next(" + g + ") :=\ncase\n" +
                                //Stabilité
                                g + " =  F_" + g + " : " + g + " ;\n" +
                                //Evolution
                                " TRUE : {" + g.min() + ", " + g.max() + "} ;\n" +
                                "esac;\n\n";
                    else
                        res += "next(" + g + ") :=\ncase\n" +
                                //Stabilité
                                g + " = F_" + g + " : " + g + " ;\n" +
                                //Augmentation
                                g + " < F_" + g + " : {" + g + ", " + g + " + 1} ;\n" +
                                //Diminution
                                g + " > F_" + g + " : {" + g + " - 1, " + g + "} ;\n" +
                                "esac;\n\n";
                }
            }
            ASSIGN = res;
        }

        // si on a plusieurs CTL, on ré-écrit plusieurs CTL
        // NuSMV fait le and de toutes les formules
        // on fait confiance à NuSMV pour qu'il traite le and au mieux
        private static void computePhi(List<Formula> ctl) throws Exception {
            String res = "";
            int i=0;
            for (Formula f : ctl) {
                res += "\nCTLSPEC NAME phi_" + i + " := " + f + " ;\n";
                i++;
            }
            PHI = res;
        }


        // si on a plusieurs CTL, on ré-écrit plusieurs CTL
        // NuSMV fait le and de toutes les formules
        // on fait confiance à NuSMV pour qu'il traite le and au mieux
        private static void computeFairPhi(List<Formula> ctl) throws Exception {
            String res = "";
            int i=0;
            for (Formula f : ctl) {
                res += "\nCTLSPEC NAME fair_phi_" + i + " := " + f.fairBio() + " ;\n";
                i++;
            }
            FAIR_PHI = res;
        }


        private static void computeNotPhi(List<Formula> ctl) throws Exception {
            String res = "";
            int i=0;
            for (Formula f : ctl) {
                res += "\nCTLSPEC NAME not_phi_" + i + " := " + f.negate().toString() + " ;\n";
                i++;
            }
            NOT_PHI = res;
        }

        //
        private static void computeNotFairPhi(List<Formula> ctl) throws Exception {
            String res = "";
            int i=0;
            for (Formula f : ctl) {
                res += "\nCTLSPEC NAME not_fair_phi_" + i + " := " + f.fairBio().negate().toString() + " ;\n";
                i++;
            }
            FAIR_NOT_PHI = res;
        }

        private static void computeEnvVar(ArrayList<Var> env) throws Exception {
            if (env.size() != 0) {
                String res = "";
                for (Var ev : env) {
                    res += "\n" + ev.toString() + ": " + ev.getLevel() + ".." + ev.getLevel() + " ;\n";
                }
                ENV_VAR = res;
            }
        }

        private static void computeInit(List<String> initConds) throws Exception {
            if (initConds!=null) {
                for (String s : initConds) {
                    INIT+="INIT\n" + s + ";\n";
                }
            }
        }

    }
}

    
