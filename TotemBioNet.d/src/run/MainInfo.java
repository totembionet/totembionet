package run;

import util.Out;

import java.util.ArrayList;

/***
 * information about the main
 * @author helen
 */
public class MainInfo {

    String fromFile;
    String inputFile;
    String compareFile;
    String path;
    String inputRoot;
    ArrayList<String> opts;

    MainInfo() {
        opts = new ArrayList<>();
    }

    boolean isUnstableSimpleOption() {
        return opts.contains("-cfc")|| opts.contains("-circuit");
    }

    boolean isUnstableRunOption() {
        return opts.contains("-stg")|| opts.contains("-pstg");
    }

    boolean isNotNetNeeded() {
        return opts.contains("-yed") ||opts.contains("-help")||opts.contains("-intersection")||opts.contains("-union")||opts.contains("-compare");
    }

    boolean isCombinationOp() {
        return opts.contains("-intersection")||opts.contains("-union");
    }

    int verboseLevel() {
        int i = opts.indexOf("-verbose");
        if (i < 0)
            return 0;
        return Integer.valueOf(opts.get(i+1));
    }

    public String getPath() {
        return path;
    }
    public String getInputFile() {
        return inputFile;
    }

    public String getInputRoot() {
        return inputRoot;
    }

}
