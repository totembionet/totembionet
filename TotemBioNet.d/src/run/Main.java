package run;

import front.yedParser.Parser;
import mddapi.apitotem.MDDFacade;
import util.Out;
import util.TotemBionetException;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * @author Hélène Collavizza
 */
public class Main {

    public static MainInfo info = new MainInfo();

    enum Options {
        yed, o, verbose, paras, help, json, simu, csv, debug, stg, pstg, cfc, circuit, intersection, union, from, compare;
    }

    public static String fromFile() {
        return info.fromFile;
    }

    // by default type is OK, except -from KO has been used
    public static String fromType() {
        if (info.opts.contains("KO"))
            return "KO";
        return "OK";
    }



    private static void printHelp() {
        System.out.println("TotemBioNet V2.3.1");
        System.out.println("******************\n");
        System.out.println("Parameter identification: ");
        System.out.println("   ./totembionet <input>.smb <options>");
        System.out.println("   Input : <input>.smb contains influence graph and (possibly) information on parameters, temporal properties, Hoare triple");
        System.out.println("   Output : <input>.out contains two sets of pameterizations : those that satisfy the properties and the others.");
        System.out.println("   Option details");
        System.out.println("      -o name    : Generate the output in <name>.out ");
        System.out.println("      -csv       : Generate also the output in a CSV file <output>.csv");
        System.out.println("      -verbose n : Verbosity level. 1: more information is printed, 2: auxilliary files (for NuSMV and Hoare) are not deleted");
        System.out.println("\nInformation on the BRN: ");
        System.out.println("   ./totembionet <input>.smb <options>");
        System.out.println("   Option details");
        System.out.println("      -paras     : Give the set of all effective parameters");
        System.out.println("      -simu      : To start simulation mode (without Hoare, without environment variables, not maintained)");
        System.out.println("\nTranslation to json / from yed");
        System.out.println("  ./totembionet <input>.smb -json     : Translate <input>.smb file in a JSON format <input>.json");
        System.out.println("  ./totembionet <input>.graphml -yed  : Translate <input>.graphml into <input>FromYed.smb.");
        System.out.println("       <input>.graphml contains BRN information built from yEd using some convention ");
        System.out.println("\n./totembionet -help : this help guide.");
        System.out.println("\nMDD Management");
        System.out.println("  ./totembionet <input>.smb -from <from>.csv        : do the enumeration from OK models in <from>.csv (<from>.csv must have been generated with -verbose 2 -csv in a preceeding run of totembionet)");
        System.out.println("  ./totembionet <input>.smb -from <from>.csv KO    : do the enumeration from KO models in <from>.csv (<from>.csv must have been generated with -verbose 2 -csv in a preceeding run of totembionet)");
        System.out.println("  ./totembionet <path> -intersection    : output in <path>/intersection.csv the intersection of all the OK models in the .csv files contains in directory <path>");
        System.out.println("  ./totembionet <path> -intersection KO : output in <path>/intersection.csv the intersection of all the KO models in .csv files contains in directory <path>");
        System.out.println("  ./totembionet <path> -union    : output in <path>/union.csv the union of all the OK models in the .csv files contains in directory <path>");
        System.out.println("  ./totembionet <path> -union KO : output in <path>/union.csv the union of all the KO models in .csv files contains in directory <path>");
	System.out.println("  ./totembionet <path>/f1.csv <path>/f2.csv -compare  : compare OK models in f1.csv and f2.csv which are previous outputs of TotemBioNet with possibly don't care values. "+
                "If the files differ, the difference are written in f1-notIn-f2.csv and f2-notIn-f1.csv in directory <path>");

        // to comment for public git
        //printUnstableHelp();
    }

    // don't call when pushing in public git
    private static void printUnstableHelp() {
        System.out.println("\nGeneration of the state transition graphs");
        System.out.println("  ./totembionet <input>.smb -stg     : Generate the SYNchronous and ASYNchronous state transition graph into <input>-stg.json file in a JSON format");
        System.out.println("  ./totembionet <input>.smb -pstg    : Generate the Parametric asynchronous state transition graph into <input>-stg.json file in a JSON format");
        System.out.println("\nFind all elementary circuits of an influence graph");
        System.out.println("  ./totembionet -circuit <filepath>/<file>.json     : find all circuits of the influence graph specified by the json file, results saved in <file>-circuits.out");
    }

    private static void printEasternEggs(){
        System.out.println("\n\n   @@@@@@@@@@@___ooO00-()-00Ooo___@@@@@@@@@@@");
        System.out.println("   @@@@@@@@@@@___ooO00-()-00Ooo___@@@@@@@@@@@.\n\n\n");
    }

    public static void main(String[] args) throws Exception {

        // analyze args and create info
        buildMainInfo(args);

        printBanner();

        // case where the net is not needed
        if (info.isNotNetNeeded()) {
            if (info.opts.contains("-yed")) {
                if (!info.inputFile.endsWith(".graphml"))
                    throw new TotemBionetException("The input file must be a .graphml file");
                Parser yedParser = new Parser();
                yedParser.generateSMBFiles(info.path, info.inputRoot);
            }
            else if (info.opts.contains("-help")) {
                printHelp();
            }
            // MDD operations
            else if (info.opts.contains("-intersection") || info.opts.contains("-union")|| info.opts.contains("-compare")) {
                manageMDD();
            }
        }
        else {
            // need the net to run
            manageNeedRun(!info.isUnstableRunOption());
        }
    }

    private static void printBanner() {
        System.out.println("\n**********************************************************");
        System.out.println("*******             TotemBioNet V2.3.2             *******");
        System.out.println("******* https://gitlab.com/totembionet/totembionet *******");
        System.out.println("**********************************************************");
    }


     private static void buildMainInfo(String[] args) throws TotemBionetException{
        //Présence d'une entrée
        if (args.length == 0)
            throw new TotemBionetException("No input, use -help");

        //System.out.println(optionList);
        String input="";
        //OPTIONS
        int i = 0;
        // scan arg list and get consecutive args when needed
        while (i < args.length) {
            if (args[i].equals("-oeuf")) {
                printEasternEggs();
                System.exit(0);
            }
            if (args[i].startsWith("-")) {
                    try {
                        Options.valueOf(args[i].substring(1));
                    }catch(IllegalArgumentException e) {
                        throw new TotemBionetException("Unknown option " + args[i]);
                    }
                info.opts.add(args[i]);
            } else if (i > 0 && (args[i - 1].equals("-verbose") || args[i - 1].equals("-o"))) {
                info.opts.add(args[i]);
            } else if (i > 0 && ((args[i - 1].equals("-intersection") || args[i - 1].equals("-union")) &&
                    (args[i].equals("OK") | args[i].equals("KO")))) {
                info.opts.add(args[i]);
            }  else if (i > 0 && args[i - 1].equals("-compare")){
                info.compareFile = args[i];
                int n = args[i].lastIndexOf("/");
                if (n!=-1)
                    info.compareFile = args[i].substring(n+1);
            } else if (i > 0 && (args[i - 1].equals("-from"))) {
                info.fromFile = args[i];
                if (i+1 < args.length && (args[i+1].equals("OK") | args[i+1].equals("KO"))) {
                    info.opts.add(args[++i]);
                }
            } else {
                input = args[i];
            }
            i++;
        }
        makePath(input);
       // System.out.println("OPTS " + info.opts);

    }

    // to cut the file name
    private static void makePath(String input) {

        if (info.isCombinationOp()) {
            info.path = input;
            if (!input.endsWith("/"))
                info.path += "/";
        }
        else {
            int slash = input.lastIndexOf("/");
            if (slash != -1 && slash != input.length()) {
                info.path = input.substring(0, slash + 1);
                info.inputFile = input.substring(slash + 1);
                int dot = info.inputFile.lastIndexOf(".");
                if (dot != -1)
                    info.inputRoot = info.inputFile.substring(0, dot);
            }
            else if (slash==-1) {
                info.path = "./";
                info.inputFile = input;
                int dot = info.inputFile.lastIndexOf(".");
                if (dot != -1)
                    info.inputRoot = info.inputFile.substring(0, dot);
            }
        }
    }


    // options that require to run teh net
    private static void manageNeedRun(boolean stable) throws Exception {
        if (!info.inputFile.endsWith(".smb"))
            throw new TotemBionetException("The input file must be a .smb file (found:" + info.inputFile + ")");
        if (info.opts.contains("-from")) {
            if (!info.fromFile.endsWith(".csv"))
                throw new TotemBionetException("The file from input models must be a .csv file");
        }
        Run run = new Run(stable,info);
        run.run();
    }


    // options with MDDs
    private static void manageMDD() throws IOException, TotemBionetException {
        Out.setVerb(info.verboseLevel());
	       if (info.opts.contains("-compare")) {
            MDDFacade.compare(info.path, info.compareFile, info.inputFile);
        }
        else {
        MDDFacade.Operation op = (info.opts.contains("-intersection")) ? MDDFacade.Operation.intersection : MDDFacade.Operation.union;
        String type = (info.opts.contains("KO")) ? "KO" : "OK";
        MDDFacade.combineAll(info.path, op, type);
	}
    }

    public static boolean isFrom() {
        return info.fromFile!=null;
    }
}



