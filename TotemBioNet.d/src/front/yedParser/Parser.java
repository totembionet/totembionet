package front.yedParser;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import front.smbParser.Regul;
import front.smbParser.Var;
import front.smbParser.VarType;
import org.w3c.dom.*;
import util.TotemBionetException;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * Parseur de fichiers format .graphml avec des conventions
 * Un simple parcours de XML
 *
 * @author Sokhna SECK [ndeye-sokhna.seck@outlook.fr]
 * @author helen
 *
 */

public class Parser {

    private boolean hasEnvVar;
    private Map<String, Var> varMap;
    private Map<String, Var> envVarMap;
    private Map<String, Regul> regulMap;
    private Map<String, String> symbolMap;
    private Map<String,Integer> generatedRegNames;

    public Parser(){
        hasEnvVar = false;
        varMap = new HashMap<>();
        envVarMap = new HashMap<>();
        regulMap = new HashMap<>();
        symbolMap = new HashMap<String, String>(){
            {
                put("&gt;", ">");
                put("&lt;", "<");
            }
        };
        generatedRegNames=new HashMap<String,Integer>();
    }

    public boolean isHasEnvVar() {
        return hasEnvVar;
    }

    public String replaceSymbols(String value){

        for(String symbol : symbolMap.keySet()){
            try{
                String s1 = symbol;
                byte[] bytes = s1.getBytes("UTF-8"); // Charset to encode into
                symbol= new String(bytes, "ASCII");
            }catch (Exception e){
                e.printStackTrace();
            }

            if(value.contains(symbol)){
                String[] result = value.split(symbol);
                value = String.join(symbolMap.get(symbol), result);
                value = value.substring(1, value.length()-1);
            }
        }

        return value;
    }

    //**************************  Nodes ************************************//
    private void parseNode(Document doc) throws Exception {
        NodeList nList = doc.getElementsByTagName("node"); //filters all nodes

        String datas="";

        for (int temp = 0; temp < nList.getLength(); temp++) {
            Element node = (Element) nList.item(temp);

            for (int i = 0; i < node.getElementsByTagName("data").getLength(); i++) {
                Element dataNode = (Element) node.getElementsByTagName("data").item(i);
                Element shapeNode = (Element) dataNode.getElementsByTagName("y:ShapeNode").item(0);

                if (node.getElementsByTagName("data").getLength() > 1) {
                    datas += " " + node.getAttribute("id");
                }

                if (shapeNode != null) {  //Il s'agit d'une variable, Shape type == ellipse (tjrs)

                    Element fill = (Element) shapeNode.getElementsByTagName("y:Fill").item(0);
                    String id = node.getAttribute("id");
                    String name = (shapeNode.getElementsByTagName("y:NodeLabel").item(0)).getTextContent();
                    if (name.contains("(")) {
                        name = name.split("[(]")[0];
                        name = name.substring(0, name.length() - 1);
                    }

                    if (fill.hasAttribute("color")) {
                        varMap.put(id, new Var(VarType.VAR, name, 0, 0));
                    } else {
                        envVarMap.put(id, new Var(VarType.ENV_VAR, name, 0, 0));
                    }

                } else { //Il s'agit d'une régulation

                    Element genericNode = (Element) dataNode.getElementsByTagName("y:GenericNode").item(0);
                    // pour éviter les problèmes de data vides liés aux opérations annulées dans yEd
                    if (genericNode != null) {
                        NodeList nodeList = genericNode.getElementsByTagName("y:NodeLabel");

                        if (nodeList.getLength() > 1) {
                            Element id = (Element) nodeList.item(0);
                            Element prop = (Element) nodeList.item(1);
                            String property = prop.getTextContent();
                            property = replaceSymbols(property);
                            property = property.replaceAll("\\n", "");
                            property = property.replaceAll("\\t", "");
                            property = property.replaceAll("\\r", "");
                            property = property.replaceAll(" ", "");

                            String regName = id.getTextContent();
                            if (regName.contains("(")) {
                                regName = regName.split("[(]")[0];
                                regName = regName.substring(0, regName.length() - 1);
                            }
                            regulMap.put(node.getAttribute("id"), new Regul(regName, new ArrayList<>(), property, null));
                        }
                    }
                }
            }
        }
        if (datas.length()!=0) {
            String message = "More than one data in node(s) " + datas+
                    "\n certainly due to cancelled operations in yEd editor.\n";
            System.err.println(message);
        }
        this.hasEnvVar = envVarMap.size() > 0;
    }

    //**************************  Edges ************************************//
    private void parseEdge(Document doc) throws Exception {
        NodeList eList = doc.getElementsByTagName("edge"); //Filters all edges to detect regulation targets

        String datas = "";

        for (int temp = 0; temp < eList.getLength(); temp++) {
            Element edge = (Element) eList.item(temp);

            if (edge.getElementsByTagName("data").getLength() > 1) {
                datas += " " + edge.getAttribute("id");
            }

            for (int i = 0; i < edge.getElementsByTagName("data").getLength(); i++) {
                Element dataNode = (Element) edge.getElementsByTagName("data").item(i);
                NodeList lineSubEdge = (dataNode.getElementsByTagName("y:PolyLineEdge").getLength() > 0) ?
                        dataNode.getElementsByTagName("y:PolyLineEdge") :
                        ((dataNode.getElementsByTagName("y:ArcEdge").getLength() > 0) ?
                                dataNode.getElementsByTagName("y:ArcEdge") : null);

                if (lineSubEdge != null) { //PolyLineEdge or ArcEdge

                    Element subEdgeNode = (Element) lineSubEdge.item(0);

                    if (subEdgeNode.getElementsByTagName("y:LineStyle").getLength() > 0) {
                        Element line = (Element) subEdgeNode.getElementsByTagName("y:LineStyle").item(0);

                        // on traite seulement les arcs dashed ou line
                        String lineType = line.getAttribute("type");
                        if (!("dashed".equals(lineType) | "line".equals(lineType))) {
                            System.err.println("Unknown line type " + lineType + "Edge has not been translated");
                        }
                        else {
                            // la source
                            String sourceId = edge.getAttribute("source");
                            Var varSource = null;

                            // on récupère la variable
                            if (varMap.keySet().contains(sourceId)) {
                                varSource = varMap.get(sourceId);
                            } else if (envVarMap.keySet().contains(sourceId))
                                varSource = envVarMap.get(sourceId);

                            // dashed donc arc (gene,multiplexe)
                            // elle a un successeur de plus
                            if ("dashed".equals(lineType)) {
                                if (varSource == null) {
                                    String message = "In edge " + edge.getAttribute("id") + "\n";
                                    message += "Source variable " + sourceId + " is not a variable but the edge is dashed";
                                    throw new TotemBionetException(message);
                                }
                                varSource.setMax(varSource.getMax() + 1);
                            }

                            // la target
                            String targetId = edge.getAttribute("target");
                            Var varTarget = varMap.keySet().contains(targetId) ? varMap.get(targetId) : null;
                            if (envVarMap.keySet().contains(targetId)) {
                                System.err.println(envVarMap.get(targetId).toBack() + ": an ENV_VAR variable can't be the target of a regulation ");
                                System.err.println("     Edge has not been translated");
                            }

                            // ligne pleine donc soit (gene,gene) soit (multiplexe,gene)
                            if (line.getAttribute("type").equals("line")) {

                                // to getWithStringId the threshold (text label of the edge)
                                String threshold = "";
                                if (subEdgeNode.getElementsByTagName("y:EdgeLabel").getLength() > 0) {
                                    Element x = (Element) subEdgeNode.getElementsByTagName("y:EdgeLabel").item(0);
                                    threshold = x.getTextContent().trim();
                                    // il y a une étiquette sur l'arc donc c'est un arc (gene,gene) avec threshold
                                    if (threshold.length() != 0 && varSource != null && varTarget != null) {
                                        // on a un successeur de plus
                                        varSource.setMax(varSource.getMax() + 1);
                                        String regName = varSource.toBack() + "_To_" + varTarget.toBack();
                                        // on vérifie si ce nom existe déjà car il peut y avoir des multi-arcs
                                        if (generatedRegNames.containsKey(regName)) {
                                            int number = generatedRegNames.get(regName);
                                            regName+="_"+number;
                                            generatedRegNames.put(regName,number+1);
                                        }
                                        else
                                            generatedRegNames.put(regName,1);
                                        String seuil = threshold.substring(1);
                                        String property = "(" + varSource.toBack() + ">=" + seuil + ")";
                                        if (threshold.charAt(0) == '-')
                                            property = "!" + property;

                                        Regul r = new Regul(regName, new ArrayList<>(), property, null);
                                        r.addTarget(varTarget.toBack().toString());
                                        regulMap.put(edge.getAttribute("id"), r);
                                    }
                                }

                                // pas de threshold donc arc (multiplexe,gene)
                                //S'il s'agit d'une régulation vers ce gène, ajouter le gène aux cibles
                                if (regulMap.keySet().contains(sourceId)) {
                                    Regul regul = regulMap.get(sourceId);
                                    String target = "";

                                    if (varMap.keySet().contains(targetId))
                                        target = varMap.get(targetId).getId();
                                    if (envVarMap.keySet().contains(targetId))
                                        target = envVarMap.get(targetId).getId();

                                    regul.addTarget(target);
                                }
                            }
                        }
                    }
                }
            }
        }
        if (datas.length() != 0) {
            String message = "WARNING : More than one data in edge(s) " + datas +
                    "\n certainly due to cancelled operations in yEd editor.\n";
            System.err.println(message);
        }
    }

    // main method to parse
    public void parse(String inputFileName) throws Exception{

        try {

            File inputFile = new File(inputFileName);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();

            parseNode(doc);
            parseEdge(doc);

        } catch (IOException e) {
            throw new TotemBionetException("NoSuchFile: " + inputFileName);
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new TotemBionetException("yEd parser : Error when getting the DocumentBuilder");
        }
    }

    //Write Vars
    public String varToSMB(){
        String varBloc = "VAR\n\n";
        varBloc+="# Generated domains are 0..d+(x) where d+(x) is the outgoing degree of x\n\n";
        for(Var var : varMap.values()){
            varBloc += var.getId() + " = " + var.getMin() + ".." + var.getMax() + " ;\n";
        }
        varBloc += "\n";
        return varBloc;
    }

    public String envVarToSMB(){
        String envVarBloc = "ENV_VAR\n\n";
        //For each Env Var, write
        for(Var var : envVarMap.values()){
            envVarBloc += var.getId() + " = " +
                    //var.getMax() + //Let empty - to create an error for the user to complete it
                                            " ;# intentionally left empty, please give a value \n\n";
        }
        envVarBloc += "\n";
        return envVarBloc;
    }

    //Write regulations
    public String regToSMB(){
        String regBloc = "REG\n\n";

        //For each Env Var, write
        for(Regul regul : regulMap.values()){
            regBloc += regul.getId() + " [" + regul.getFormulaString() + "] => ";
            for(String target : regul.getTargets()){
                regBloc += target + " ";
            }
            regBloc += "; \n\n";
        }

        regBloc += "\n";
        return regBloc;
    }

    // File generation
    public void generateSMBFiles(String path,String inputfileName) {
        String prefixName = path + inputfileName;

        try {
            String outputGraphFile = prefixName + "FromYed.smb";
            if (new File(outputGraphFile).exists()) {
                String response = "";
                Scanner scanner = new Scanner(System.in);
                System.out.print(outputGraphFile + " already exists. Do you want to overwrite it ? y/n : ");
                response = scanner.next();
                if (response.equals("n")) {
                    System.out.print("Please give a file name <name>");
                    System.out.print("    The file will be generated in " + path + "<name>GraphFromYed.smb ");
                    response = scanner.next();
                    outputGraphFile = path + response + "GraphFromYed.smb";
                }
                scanner.close();
            }

            String yedFile = path + inputfileName + ".graphml";
            System.out.println(" *** Starting generation of .smb file from " + yedFile + "***");
            this.parse(yedFile);
            String result = "#*****************************\n# .smb file generated from yEd file " + yedFile + "\n#*****************************\n\n";
            if (hasEnvVar)
                result += this.envVarToSMB(); //Adds envVar to inputGraph.smb file
            result += this.varToSMB();
            result += this.regToSMB();

            BufferedWriter out = new BufferedWriter(new FileWriter(outputGraphFile));
            out.write(result);
            out.write("# end of graph generated from Yed\n\nEND");

            out.close();
            System.out.println(" *** Success *** File has been generated in " + outputGraphFile + "*** ");
            if (this.isHasEnvVar()) {
                System.out.println("Warning : " + outputGraphFile + " contains environment variables.");
                System.out.println("          Their domains have been intentionally left empty, please complete them.");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static void main(String... args){
        Parser parser = new Parser();
        parser.generateSMBFiles("./",args[0]);
    }

}
