package front.smbParser;

import back.logic.CTLlogic.*;
import back.logic.Formula;
import back.logic.blogic.*;

/**
 * @author Hélène Collavizza
 */

enum CtlOpType {
    IMPLICATION, OP_BOOL, TEMP_CTL, TEMP_CTL_U, ATOME;
}
public class Ctl {
    private CtlOpType opType;
    private String operateur;
    private Ctl expr1;
    private Ctl expr2;

    private Var var;
    private String constant;

    public Ctl(CtlOpType opType,String operateur,Ctl expr1,Ctl expr2,Var var,String constant) {
        this.opType = opType;
        this.operateur = operateur;
        this.expr1 = expr1;
        this.expr2 = expr2;
        this.var = var;
        this.constant=constant;
    }

    public Ctl(CtlOpType opType,String operateur,Ctl expr1,Ctl expr2) {

        this(opType,operateur,expr1,expr2,null,null);
    }

    public Ctl(CtlOpType opType,String operateur,Var var,String constant) {
        this(opType,operateur,null,null,var,constant);
    }

    public Ctl(CtlOpType opType,String operateur,Ctl expr1) {
        this(opType,operateur,expr1,null);
    }

    public Formula toBack(){
        switch(operateur){
            /*******  BOOLEAN  *********/
            case "->":{
                return new Imply(expr1.toBack(), expr2.toBack());
            }
            case "&":{
                return  new And(expr1.toBack(), expr2.toBack());
            }
            case "|":{
                return  new Or(expr1.toBack(), expr2.toBack());
            }
            case "!":{
                return  new Not(expr1.toBack());
            }
            /*******  CTL PROP  *********/
            case "EX":{
                return  new ExistNeXt(expr1.toBack());
            }
            case "AX":{
                return  new AllNeXt(expr1.toBack());
            }
            case "EF":{
                return  new ExistFuture(expr1.toBack());
            }
            case "AF":{
                return  new AllFuture(expr1.toBack());
            }
            case "EG":{
                return  new ExistGenerally(expr1.toBack());
            }
            case "AG":{
                return new AllGenerally(expr1.toBack());
            }
            case "E":{
                return  new ExistUntil(expr1.toBack(), expr2.toBack());
            }
            case "A":{
                return  new AllUntil(expr1.toBack(), expr2.toBack());
            }
            /*******  COMPARISON  *********/
            case "<=": {
                return  new LessEq(var.toBack(), new Int(Integer.parseInt(constant)));
            }

            case ">=":{
                return  new GreatEq(var.toBack(), new Int(Integer.parseInt(constant)));
            }
            case "<":{
                return  new Less(var.toBack(), new Int(Integer.parseInt(constant)));
            }
            case ">":{
                return  new Great(var.toBack(), new Int(Integer.parseInt(constant)));
            }
            case "=":{
                return  new Equa(var.toBack(), new Int(Integer.parseInt(constant)));
            }
            default: {
                System.err.println("CtlProp to Formula conversion : Unknown operator");
            }
        }
        return null;
    }

    public CtlOpType getOpType() {
        return opType;
    }

    public String getOperateur() {
        return operateur;
    }

    public Ctl getExpr1() {
        return expr1;
    }

    public Ctl getExpr2() {
        return expr2;
    }

    public Var getVar() {
        return var;
    }

    public String getConstant() {
        return constant;
    }

    public void setVar(Var var) {
        this.var = var;
    }

    public String toJSON() {
        String formula = "{";
        boolean isExpr2 = expr2 != null;
        boolean isAtome = opType == CtlOpType.ATOME;
        formula += "\"type\" : \"" + opType.name() +
                "\",\"operateur\" : \"" + operateur +
                "\",\"expr1\" : " + ((isAtome) ? "\"" + var.getId() + "\"" : expr1.toJSON()) +
                ",\"expr2\" : " + ( (isAtome) ? "\"" + getConstant() + "\""    : ((isExpr2) ?  expr2.toJSON() : "\"null\""));
        formula += "}";
        return formula;
    }
}
