package front.smbParser;

import back.net.*;

/**
 *
 * @author Sokhna SECK [ndeye-sokhna.seck@outlook.fr]
 *
 */

public class Var {
    private VarType varType;
    private String id;
    private Integer min;
    private Integer max;
    private boolean snoussi;
    private Gene varGene;

    public Var(VarType varType, String id, Integer min, Integer max,boolean snoussi) {
        this.varType = varType;
        this.id = id;
        this.min = min;
        this.max = max;
        this.snoussi =snoussi;
        varGene = null;
    }

    // par défaut, la variable est Snoussi
    public Var(VarType varType, String id, Integer min, Integer max) {
        this(varType,id,min,max,true);
    }

    public VarType getVarType() {
        return varType;
    }

    void setVarType(VarType vt) {
        varType = vt;
    }

    public boolean isEnvVar(){
        return varType==VarType.ENV_VAR;
    }

    public String getId() {
        return id;
    }

    public Integer getMin() {
        return min;
    }

    public Integer getMax() {
        return max;
    }

    public void setMin(Integer min) {
        this.min = min;
    }

    public void setMax(Integer max) {
        this.max = max;
    }

    public Gene toBack(){
        if (varGene==null)
            varGene = new Gene(this.id, this.min, this.max,this.snoussi);
        return varGene;
    }
}
