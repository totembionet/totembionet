package front.smbParser;

import back.logic.Formula;

/**
 *
 * @author Sokhna SECK [ndeye-sokhna.seck@outlook.fr]
 *
 */

 enum CtlType {
    CTL,FAIR_CTL;
}

public class CtlProp {

    private String name;
    private CtlType type;
    private Ctl ctl;

    public CtlProp(CtlType type,String name,Ctl ctl) {
        this.type = type;
        this.ctl=ctl;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public CtlType getType() {return type;}

    public CtlOpType getOpType() {
        return ctl.getOpType();
    }

    public String getOperateur() {
        return ctl.getOperateur();
    }

    public Var getVar() {
        return ctl.getVar();
    }

    public String getConstant() {
        return ctl.getConstant();
    }

    public Ctl getExpr1(){
        return ctl.getExpr1();
    }

    public Ctl getExpr2(){
        return ctl.getExpr2();
    }

    public String toJSON() {
        return ctl.toJSON();
    }

    public Formula toBack(){
        return ctl.toBack();
    }
}