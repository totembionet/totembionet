package front.smbParser;

import back.net.*;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author Sokhna SECK [ndeye-sokhna.seck@outlook.fr]
 * @author helen
 *
 */

public class VarBlock {

    private List<Var> data;

    public VarBlock() {
        this.data = new ArrayList<>();
    }

    public List<Var> getData() {
        return data;
    }

    private Stream<Var> filterVar(String varId) {
        return data.stream().filter(var -> var.getId().equals(varId));
    }

    public boolean existsVar(String varId){
        Stream<Var> varStream = filterVar(varId);
        int count = (int) varStream.count();
        if (count>1)
            System.err.println("Var " + varId + ": " + count + " var with this id.");
        return count==1;
    }

    public void addVar(VarType type, String id, Integer valMin, Integer valMax , boolean snoussi) {
        this.data.add(new Var(type, id, valMin, valMax,snoussi));
    }

    public Var getVarWithId(String id){
        Stream<Var> varStream = filterVar(id);
        return (new ArrayList<Var>(varStream.collect(Collectors.toList()))).get(0);
    }

    public String toJSON(){
        long env_varCount = data.stream().filter(var -> var.getVarType() == VarType.ENV_VAR).count();
        long varCount =  data.stream().filter(var -> var.getVarType() == VarType.VAR).count();
        String env_varBlock = "{\"blocktype\" : \"env_var\""
                             +", \"value\" : {\"count\" : "+ env_varCount
                                           +" , \"data\" : [";

        String varBlock = ",{\"blocktype\": \"var\""
                        + " , \"value\" : {\"count\" : "+ varCount
                                  +" , \"data\" : [";

        for(Var var : this.data){
            if(var.getVarType() == VarType.ENV_VAR){
                env_varBlock += "{ \"id\" : \"" + var.getId() + "\", \"val\" : " + var.getMin() + "},";
            }else {
                varBlock += "{ \"id\" : \"" + var.getId() + "\", \"min\" : " + var.getMin() + ", \"max\" : " + var.getMax() + "},";
            }
        }
        if(env_varCount > 0)
            env_varBlock = env_varBlock.substring(0, env_varBlock.length() - 1);
        env_varBlock += "]}}";

        if(varCount > 0)
            varBlock = varBlock.substring(0, varBlock.length() - 1);
        varBlock += "]}}";
        return env_varBlock + varBlock;
    }

    public List<Gene> getBackFormat(){
        List<Gene> geneList = new ArrayList<>();
        for(Var var : this.data){
            geneList.add(var.toBack());
        }
        return geneList;
    }


}