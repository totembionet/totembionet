package front.smbParser;

/**
 *
 * @author Sokhna SECK [ndeye-sokhna.seck@outlook.fr]
 *
 */

public enum VarType {
    ENV_VAR,
    INIT_VAR,
    VAR;
}
