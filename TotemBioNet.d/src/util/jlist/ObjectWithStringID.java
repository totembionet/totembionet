package util.jlist;

public interface ObjectWithStringID {
     String stringId();
}
