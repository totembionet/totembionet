package back.logic.blogic;

import back.logic.Formula;
import util.TotemBionetException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author Adrien Richard
 * @author Hélène Collavizza
 */

public class And extends BooleanFormula {

    public And(Formula left, Formula right) {
        super(left, right);
    }

    public int eval() {
        if (getLeft().eval() > 0 && getRight().eval() > 0)
            return 1;
        return 0;
    }

    public int eval(HashMap<String, Integer> state) {
        if (getLeft().eval(state) > 0 && getRight().eval(state) > 0)
            return 1;
        return 0;
    }

    public String toString() {
        return "(" + getLeft() + Formula.AND + getRight() + ")";
    }

    @Override
    public Formula fairBio() {
        return new And(getLeft().fairBio(), getRight().fairBio());
    }

    @Override
    public String toJsonString() {
        return "{\"op\":" + "\"AND\"" + ",\"left\":" + getLeft().toJsonString() + ",\"right\":" + getRight().toJsonString() +"}";
    }

//    @Override
//    public Formula negate() {
//        if (!(getLeft() instanceof Imply) && !(getRight() instanceof Imply))
//            return super.negate();
//        if (getLeft() instanceof Imply)
//            ((Imply) getLeft()).setRoot();
//        if (getRight() instanceof Imply)
//            ((Imply) getRight()).setRoot();
//        return new Or(getLeft().negate(),getRight().negate());
//    }

    // @author Hélène Collavizza
    @Override
    public List<Formula> toSetOfConjunct() throws TotemBionetException {
        ArrayList<Formula> conjonct = new ArrayList<Formula>();
        conjonct.addAll(getRight().toSetOfConjunct());
        conjonct.addAll(getLeft().toSetOfConjunct());
        return conjonct;
    }

    @Override
    public String toYices() {
        return "(and " + getLeft().toYices() + " " + getRight().toYices() + ")";
    }

    @Override
    public Formula translateCTL() {
        return new And(getLeft().translateCTL(),getRight().translateCTL());
    }

    @Override
    public Formula toCNF() {
        Formula l = getLeft().toCNF();
        Formula r = getRight().toCNF();
        if ((r instanceof Or)&&(l instanceof Or)) {
            Formula res = new And(new Or(((Or) l).getLeft(),((Or) r).getLeft()),new Or(((Or) l).getLeft(),((Or) r).getRight()));
            res = new And(res, new And(new Or(((Or) l).getRight(),((Or) r).getLeft()),new Or(((Or) l).getRight(),((Or) r).getRight())));
            res.id = Formula.CNF_COUNTER++;
            return res;
        }
        if (r instanceof Or) {
            Formula res = new And(new Or(l,((Or) r).getLeft()),new Or(l,((Or) r).getRight()));
            res.id = Formula.CNF_COUNTER++;
            return res;
        }
        if (l instanceof Or) {
            Formula res = new And(new Or(r,((Or) l).getLeft()),new Or(r,((Or) l).getRight()));
            res.id = Formula.CNF_COUNTER++;
            return res;
        }
        this.id = Formula.CNF_COUNTER++;
        return this;
    }

}