package back.logic.blogic;

import back.logic.Formula;
import util.TotemBionetException;

import java.util.HashMap;
import java.util.List;

/**
 * @author Adrien Richard
 * @author Hélène Collavizza
 */


public class Imply extends BooleanFormula {

    // vrai si => est l'opérateur de tête
    // dans ce cas, la négation de P=>Q est P=>!Q
    // TODO : vérifier en détail la négation
    private boolean isRoot;

    public Imply(Formula left, Formula right) {
        super(left, right);
        isRoot = false;
    }

    public void setRoot() {
        isRoot = true;
    }

    public int eval() {
        if (getLeft().eval() < 1 || getRight().eval() > 0)
            return 1;
        return 0;
    }

    public int eval(HashMap<String, Integer> state) {
        if (getLeft().eval(state) < 1 || getRight().eval(state) > 0)
            return 1;
        return 0;
    }

    @Override
    public Formula negate() {
        if (isRoot)
            return new Imply(getLeft(), new Not(getRight())); //getRight().negate());
        return new Not(this);
    }

    public String toString() {
        return "(" + getLeft() + Formula.IMPLY + getRight() + ")";
    }

    @Override
    public Formula fairBio() {
        return new Imply(getLeft().fairBio(), getRight().fairBio());
    }

    @Override
    public List<Formula> toSetOfConjunct() throws TotemBionetException {
        throw new TotemBionetException("Imply is not a comparator");
    }

    @Override
    public String toJsonString() {
        return "{\"op\":" + "\"IMPLY\"" + ",\"left\":" + getLeft().toJsonString() + ",\"right\":" + getRight().toJsonString() +"}";
    }

    @Override
    public String toYices() {
        return "(implies " + getLeft().toYices() + " " + getRight().toYices()+")";
    }

    @Override
    public Formula translateCTL() {
        return new Imply(getLeft().translateCTL(),getRight().translateCTL());
    }

    @Override
    public Formula toCNF() {
        return this;
    }

}
