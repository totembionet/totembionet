package back.logic.blogic;

import back.logic.Formula;

import java.util.HashMap;

/**
 * @author Adrien Richard
 * @author helen
 */

public class Add extends IntegerFormula {

    public Add(Formula left, Formula right) {
        super(left, right);
    }

    public int eval() {
        return getLeft().eval() + getRight().eval();
    }

    public int eval(HashMap<String, Integer> paraState) {
        return getLeft().eval(paraState) + getRight().eval(paraState);
    }

    public String toString() {
        return "(" + getLeft() + Formula.ADD + getRight() + ")";
    }

    @Override
    public String toJsonString() {
        return "{\"op\":" + "\"ADD\"" + ",\"left\":" + getLeft().toJsonString() + ",\"right\":" + getRight().toJsonString() +"}";
    }

    @Override
    public String toYices() {
        return "(add " + getLeft().toYices() + " " + getRight().toYices()+")";
    }
}