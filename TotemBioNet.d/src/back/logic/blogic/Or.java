package back.logic.blogic;

import back.logic.Formula;
import util.TotemBionetException;

import java.util.HashMap;
import java.util.List;

/**
 * @author Adrien Richard
 * @author Hélène Collavizza
 */


public class Or extends BooleanFormula {

    public Or(Formula left, Formula right) {
        super(left, right);
    }

    public int eval() {
        if (getLeft().eval() > 0 || getRight().eval() > 0)
            return 1;
        return 0;
    }

    public int eval(HashMap<String, Integer> state) {
        if (getLeft().eval(state) > 0 || getRight().eval(state) > 0)
            return 1;
        return 0;
    }

    public String toString() {
        return "(" + getLeft() + Formula.OR + getRight() + ")";
    }

    @Override
    public Formula fairBio() {
        return new Or(getLeft().fairBio(), getRight().fairBio());
    }

    @Override
    public List<Formula> toSetOfConjunct() throws TotemBionetException {
        throw new TotemBionetException("Or expression is not a comparator");
    }

    @Override
    public String toJsonString() {
        return "{\"op\":" + "\"OR\"" + ",\"left\":" + getLeft().toJsonString() + ",\"right\":" + getRight().toJsonString() +"}";
    }

    @Override
    public String toYices() {
        return "(or " + getLeft().toYices() + " " + getRight().toYices()+")";
    }

    @Override
    public Formula translateCTL() {
        return new Or(getLeft().translateCTL(),getRight().translateCTL());
    }

    @Override
    public Formula toCNF() {
        Formula l = getLeft().toCNF();
        Formula r = getRight().toCNF();
        Formula res = (l.id<r.id)?new Or(l,r):new Or(r,l);
        // TODO : c'est faux, il faut distribuer
        res.id = Formula.CNF_COUNTER++;
        return res;
    }

}