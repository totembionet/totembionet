package back.logic.blogic;

import back.logic.Formula;

import java.util.HashMap;

/**
 * @author Adrien Richard
 */


public class Mult extends IntegerFormula {

    public Mult(Formula left, Formula right) {
        super(left, right);
    }

    public int eval() {
        return getLeft().eval() * getRight().eval();
    }

    public int eval(HashMap<String, Integer> paraState) {
        return getLeft().eval(paraState) * getRight().eval(paraState);
    }

    public String toString() {
        return "(" + getLeft() + Formula.MULT + getRight() + ")";
    }

    @Override
    public String toJsonString() {
        return "{\"op\":" + "\"MULT\"" + ",\"left\":" + getLeft().toJsonString() + ",\"right\":" + getRight().toJsonString() +"}";
    }

    @Override
    public String toYices() {
        return "(multiply " + getLeft().toYices() + " " + getRight().toYices()+")";
    }
}