package back.logic.CTLlogic;

import back.logic.Formula;

/**
 * classe pour représenter l'opérateur CTL EG
 *
 * @author Hélène Collavizza
 */


public class ExistGenerally extends UnaryCTLFormula {

    public ExistGenerally(Formula f) {
        super(f);
    }

    public String toString() {
        return Formula.EG + "(" + getLeft() + ")";
    }

    /*@Override
    public Formula negate() {
        return new AllGenerally(getLeft().negate());
    }*/

    @Override
    public Formula fairBio() {
        Formula fairL = getLeft().fairBio();
        return new ExistUntil(fairL, new AllGenerally(fairL));
    }

    @Override
    public String toJsonString() {
        return "{\"uctl\":" + "\"EG\"" + ",\"left\":" + getLeft().toJsonString()  +"}";
    }

    @Override
    public Formula translateCTL() {
        return new ExistGenerally(getLeft().translateCTL());
    }
}
