package back.logic.CTLlogic;

import back.logic.Formula;

/**
 * classe pour représenter l'opérateur CTL EU
 *
 * @author Hélène Collavizza
 */


public class ExistUntil extends BinaryCTLFormula {

    public ExistUntil(Formula l, Formula r) {
        super(l, r);
    }

    @Override
    public String toString() {
        return "E [" + getLeft() + " U " + getRight() + "]";
    }

    @Override
    public Formula fairBio() {
        return new ExistUntil(getLeft().fairBio(), getRight().fairBio());
    }

    @Override
    public String toJsonString() {
        return "{\"bctl\":" + "\"EU\"" + ",\"left\":" + getLeft().toJsonString()  +  ",\"right\":" + getRight().toJsonString() +"}";
    }

    @Override
    public Formula translateCTL() {
        return new ExistUntil(getLeft().translateCTL(),getRight().translateCTL());
    }
}
