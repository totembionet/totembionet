package back.logic.CTLlogic;

import back.logic.Formula;

/**
 * classe pour représenter l'opérateur CTL EX
 *
 * @author Hélène Collavizza
 */


public class ExistNeXt extends UnaryCTLFormula {

    public ExistNeXt(Formula f) {
        super(f);
    }

    public String toString() {
        return Formula.EX + "(" + getLeft() + ")";
    }

    /*@Override
    public Formula negate() {
        return new AllNeXt(getLeft().negate());
    }*/

    @Override
    public Formula fairBio() {
        return new ExistNeXt(getLeft().fairBio());
    }

    @Override
    public String toJsonString() {
        return "{\"uctl\":" + "\"EX\"" + ",\"left\":" + getLeft().toJsonString()  +"}";
    }

    @Override
    public Formula translateCTL() {
        return new ExistNeXt(getLeft().translateCTL());
    }
}
