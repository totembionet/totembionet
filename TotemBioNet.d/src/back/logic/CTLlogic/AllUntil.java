package back.logic.CTLlogic;

import back.logic.Formula;
import back.logic.blogic.And;
import back.logic.blogic.Not;
import back.logic.blogic.Or;

/**
 * classe pour représenter l'opérateur CTL AU
 *
 * @author Hélène Collavizza
 */

public class AllUntil extends BinaryCTLFormula {

    public AllUntil(Formula l, Formula r) {
        super(l, r);
    }

    @Override
    public String toString() {
        return "A [" + getLeft() + " U " + getRight() + "]";
    }

    @Override
    public Formula fairBio() {
        Formula fairR = getRight().fairBio();
        Formula fairL = getLeft().fairBio();
        Formula f1 = new AllFuture(fairR);
        Formula notR = new Not(fairR);
        Formula f2 = new ExistUntil(notR, new And(new Not(fairL), notR));
        return new And(f1.fairBio(), new Not(f2.fairBio()));
    }

    @Override
    public String toJsonString() {
        return "{\"bctl\":" + "\"AU\"" + ",\"left\":" + getLeft().toJsonString() +  ",\"right\":" + getRight().toJsonString() +"}";
    }

    @Override
    public Formula translateCTL() {
        Formula r = getRight().translateCTL();
        Formula l = getLeft().translateCTL();
        Formula nr = new Not(r);
        Formula nl = new Not(l);
        return new Not(new Or(new ExistUntil(new Not(r),new And(nr,nl)),new ExistGenerally(nl)));
    }


}
