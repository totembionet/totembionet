package back.logic.CTLlogic;

import back.logic.Formula;
import back.logic.blogic.True;

/**
 * classe pour représenter l'opérateur CTL EU
 *
 * @author Hélène Collavizza
 */


public class ExistFuture extends UnaryCTLFormula {

    public ExistFuture(Formula f) {
        super(f);
    }

    public String toString() {
        return Formula.EF + "(" + getLeft() + ")";
    }

    /*@Override
    public Formula negate() {
        return new AllFuture(getLeft().negate());
    }*/

    @Override
    public Formula fairBio() {
        return new ExistFuture(getLeft().fairBio());
    }

    @Override
    public String toJsonString() {
        return "{\"uctl\":" + "\"EF\"" + ",\"left\":" + getLeft().toJsonString()  +"}";
    }

    @Override
    public Formula translateCTL() {
        return new ExistUntil(new True(),getLeft().translateCTL());
    }

}
