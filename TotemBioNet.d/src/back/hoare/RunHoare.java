package back.hoare;

import back.net.Net;
import back.search.InstanciatedSearchEngineWithHoare;
import front.hoareFol.hoareParser.Parser;
import front.smbParser.*;
import util.TotemBionetException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;


/**
 * @author Hélène Collavizza
 *
 * Class to run the Hoare triples
 * - build the OCAML files
 * - run OCAML
 * - parse the result as json file
 * - transform json result as internal Formulas
 */
public class RunHoare {
    private String fileName; // file name prefix
    private String distdir; // associated Hoare directory
    private HoareBlocks hoareBlocks; // Hoare blocks (in front syntax)

    public RunHoare(Visitor visitor, String path, String inputFile) {
        try {
            hoareBlocks = visitor.getHoareBlocks();
            // Create a temporary directory
            String hoarePath = path + "hoare/";
            new File(hoarePath).mkdirs();
            fileName = hoarePath + inputFile ;
            distdir = System.getenv("TOTEMPATH") + "/TotemBioNet.d/ressources/";
            generateBRN(visitor);
        } catch (Exception e) {
            System.out.println(e);
            System.exit(2);
        }
    }

    // Generate BRN (only once for each triple)
    private void generateBRN(Visitor visitor) throws IOException{
        FileOutputStream outstream = new FileOutputStream(new File(fileName + "-BRN.ml"));

        // Copy Ocaml core1
        insertfile(distdir + "core1.ml", outstream);

        // Generate BRN
        HoareBlocks hoareBlocks = visitor.getHoareBlocks();
        List<Var> vars = visitor.getVarBlock().getData();
        List<Regul> regs = visitor.getRegBlock().getRegs();
        hoareBlocks.generateBRNOCaml(outstream,vars,regs);

        // Copy Ocaml core2
        insertfile(distdir + "core2.ml", outstream);

        outstream.close();
    }

    // Generate the ocaml file for one triple
    private void generateTriplet(String name,OneHoareBlock ohb) throws IOException{
        FileOutputStream outstream = new FileOutputStream(new File(name));
        // copy the BRN
        insertfile(fileName + "-BRN.ml",outstream);
        // add the ocaml for the triple
        ohb.generateTripletOCaml(outstream);
        outstream.close();
    }

    // @author Erick Gallesio
    private static void insertfile(String src, FileOutputStream outstream) {
        try {
            FileInputStream instream = new FileInputStream(new File(src));
            byte[] buffer = new byte[1024];
            int length;

            while ((length = instream.read(buffer)) > 0) {
                outstream.write(buffer, 0, length);
            }
            instream.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(2);
        }
    }

    // to run all the triples and build the associated HoareTriplets
    public HoareTriplets runHoareTriplets() throws Exception {
        HoareTriplets hoareTriplets = new HoareTriplets();
        // build each triple
        for (OneHoareBlock ohb : hoareBlocks.getBlocks()) {
            // generate ocaml files
            String name = fileName + "-" + ohb.getId() ;
            System.out.println("\n* Processing of triple: " + ohb.getId());
            generateTriplet(name+  ".ml",ohb);
            // System.out.println(command);
            // run ocaml
            String command = "ocaml " + name + ".ml > " + name + ".out";
            String cmd[] = {"sh", "-c", command};
            Process proc = Runtime.getRuntime().exec(cmd);
            int exitVal = proc.waitFor();
            if (exitVal == 0) {
                // parse the json file to build a Formula
                Parser hoareParser = new Parser();
                try {
                    hoareParser.parse(name + ".out");
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    Net.inconsistentHoare=1;
                }
                if (Net.inconsistentHoare==0) {
                    String jsonFile = name + ".json";
                    if (!(new File(jsonFile)).exists())
                        throw new Exception("A problem has succeeded when creating Hoare json result file " + fileName + ".json");
                    OneHoareTriplet oht;
                    // add the triplet
                    // TODO : check if this exception can occur (or is overrided with hoareParser.parse)
                    try {
                        oht = new OneHoareTriplet(jsonFile, ohb.getId());
                        hoareTriplets.addTriplet(oht);
                    } catch (TotemBionetException e) {
                        System.out.println(e.getMessage());
                        Net.inconsistentHoare = 1;
                    }
                }
            } else {
                throw new Exception(fileName + ".out has not been generated for Hoare triple");
            }
        }
        return hoareTriplets;
    }

    public boolean canBeRun() {
        return !hoareBlocks.isEmpty();
    }
}
