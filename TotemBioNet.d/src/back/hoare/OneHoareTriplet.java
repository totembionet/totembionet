package back.hoare;

import back.logic.Formula;
import back.logic.Var;
import back.logic.blogic.BinaryConnective;
import back.logic.blogic.Comparator;
import back.logic.blogic.Int;
import back.logic.blogic.Not;
import back.net.Gene;
import back.net.Net;
import back.net.Para;
import org.json.JSONObject;
import util.Out;
import util.TotemBionetException;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * @author Hélène Collavizza
 */
public class OneHoareTriplet {

    String id ; // number of the triplet

    // NOTA : 1 liste de Formula car si on a une conjonction
    //        on la transforme en liste
    private List<Formula> formulas;

    // vrai si conjonction de conditions de la forme x op_comp val
    // (où op_comp opérateur de comparaison)
    public boolean isOnlyConjunct;

    // prend en entrée un json qui contient le calcule de wp
    // les transforme en Formula
    // positionne un flag pour savoir s'il s'agit d'une conjonction
    // d'inégalités
    // si oui, on réduit les domaines, sinon, on vérifie la formule
    // pour chaque modèle
    public OneHoareTriplet(String jsonFile, String id) throws Exception {
//        nbRemovedParam= BigInteger.valueOf(0);
        isOnlyConjunct = true;
        formulas = new ArrayList<>();
        this.id=id;
        // créé le visiteur de json
        String jsonString = null;
        try{
            byte[] encoded = Files.readAllBytes(Paths.get(jsonFile));
            jsonString = new String(encoded, StandardCharsets.US_ASCII);
        } catch(IOException io){
            io.printStackTrace();
        }

        // on s'intéresse au résultat
        JSONObject obj = new JSONObject(jsonString);
        JSONObject hoareResult = obj.getJSONObject("value").getJSONObject("res");
        Formula fh = parseJSON(hoareResult);

        // si le résultat est traitable, on décompose la conjonction
        try {
            formulas.addAll(fh.toSetOfConjunct());
        } catch (TotemBionetException e) {
            isOnlyConjunct = false;
            formulas.add(fh);
        }

    }

    // transforme une formule logique JSON issue d'une formule
    // de syntaxe HoareFol.g4 en une Formula
    private Formula parseJSON(JSONObject json) throws Exception {
        String type = json.getString("type");
        if ("unsatisfiable".equals(type)) {
            Net.inconsistentHoare=1;
            throw new TotemBionetException("Hoare triple is unsatisfiable");
        }
        String op = json.getString("op");
        Formula f = null;
        switch(type) {
            case "boolExpr": {
                Formula e1 = parseJSON(json.getJSONObject("expr1"));
                Formula e2 = parseJSON(json.getJSONObject("expr2"));
                f = BinaryConnective.makeFormula(op,e1,e2);
            }
            break;
            case "atome": {
                String varName = json.getString("id");
                // TODO : on pourrait faire mieux pour savoir qu'il s'agit d'un paramètre
                if (!varName.startsWith("K"))
                    throw new TotemBionetException("Hoare weakest precondition can't be handled: it contains variable " + varName);
                    //isOnlyPara = false;
                Formula var = new Var(varName);
                Formula value = new Int(json.getInt("value"));
                f = BinaryConnective.makeFormula(op,var,value);
            }
            break;
            case "negExpr": {
                f = new Not(parseJSON(json.getJSONObject("expr")));
            }
            break;
            default: {
                System.err.println("Error : Unknwown Expr type in JSON Hoare : " + type);
            }
            break;
        }
        return f;
    }

    // associe à chaque paramètre présent dans une contrainte de Hoare
    // sa valeur à mettre en borne max
    // si plusieurs contraintes de Hoare portent sur le même para
    // on met le min (ou le max selon la contrainte)
    public void propagateHoareConstraints(Net net) throws Exception {
        System.out.println(this);
        for (Formula f : formulas) {
            if (Out.verb()>0)
                System.out.print("   Propagating  " + f + " ");
            for (Gene g : net.genes()) {
                for (Para p : g.getParas()) {
                    try {
                        ((Comparator) f).setParaValue(p);
                    }
                    catch (TotemBionetException e) {
                        System.out.print("   Hoare constraint " + f
                                + " is inconsitent with current value of parameter " + p
                                + ". " + p.intervalsToString() + "\n");
                        if (Out.verb()>=1)
                            System.out.print(e.getMessage()+"\n");
                        // incompatible domains
                        Net.inconsistentHoare=2;
                        System.out.println(" *** Hoare failure");
                        //throw new TotemBionetException("Hoare trace is empty");
                    }
                }
            }
        }
        if (Net.inconsistentHoare==0)
            System.out.println("   Domains have been successfully reduced.");
    }


    // pour savoir si une formule est valide et donc utilisée
    // pour générer un modèle
    public boolean valid(HashMap<String, Integer> model) {
        //System.out.println(model);
        for (Formula f : formulas) {
            if (f.eval(model) == 0) {
                //System.out.println(f + " is not valid");
//                nbRemovedParam=nbRemovedParam.add(nbRemovedParam);
                return false;
            }
        }
        return true;
    }

    public boolean isEmpty() {
        return formulas.isEmpty();
    }

    public String toString() {
        String s="Triple " + id + ", ";
        s+="set of formulas: "  + formulas;
//        for (Formula f : formulas)
//            s+="     "+f;
        return s;
    }



}
