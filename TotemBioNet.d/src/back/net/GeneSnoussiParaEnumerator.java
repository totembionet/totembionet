package back.net;

import util.jlist.OrderedArrayList;
import back.search.ParaEnumerator;

/**
 * @author Hélène Collavizza: classe
 * @author Adrien Richard: algo
 */
public class GeneSnoussiParaEnumerator implements ParaEnumerator {

    private OrderedArrayList<Para> paras;
    private DagByDeep dagByDeep;
    private int currentDeep;
    private String geneName;

    public GeneSnoussiParaEnumerator(OrderedArrayList<Para> paras, DagByDeep dagByDeep, String name) {
        this.paras = paras;
        this.dagByDeep = dagByDeep;
        currentDeep = dagByDeep.size() - 1;
        geneName = name;
    }

    // méthodes  pour Snoussi
    ////////////////////////////////
    public void firstParameterization() {
        //On minimise les intervalles courants de tous les param�tres
        for (int i = 0; i < paras.size(); i++) {
            paras.get(i).firstCurrentInt();
        }
        //On maximise les intervalles max courants de tous les param�tres
        for (int i = 0; i < paras.size(); i++)
            paras.get(i).setCurrentMaxLessThanSuccs();
        //On part en bas du DAG
        currentDeep = dagByDeep.size() - 1;
    }

    private boolean nextLocalParameterization() {
        //On augmente l'intervalle courant d'un param�tre � la profondeur currentDeep
        for (int i = 0; i < dagByDeep.getLevel(currentDeep).size(); i++) {
            //Si une augmentation a lieu, on augmente currentDeep
            //d'une unit� et on maximise l'intervalle max courant des
            //param�tres � la nouvelle profondeur
            if (dagByDeep.getLevel(currentDeep).get(i).nextCurrentInt()) {
                currentDeep++;
                for (int j = 0; j < dagByDeep.getLevel(currentDeep).size(); j++)
                    dagByDeep.getLevel(currentDeep).get(j).setCurrentMaxLessThanSuccs();
                return true;
            }
        }
        //Si toutes les configurations ont �t� �puis�es � la
        //profondeur currentDeep, on diminue la profondeur
        currentDeep--;
        return false;
    }

    public boolean nextParameterization() {
        while (currentDeep >= 0)
            if (nextLocalParameterization())
                return true;
        firstParameterization();
        return false;
    }

    public String toString() {
        return "Snoussi parameter iterator for " + geneName;
    }
}
