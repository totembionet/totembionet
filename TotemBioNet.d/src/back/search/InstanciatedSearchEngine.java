package back.search;

/**
 * This class is used to search for valid models
 * All models are enumerated
 *
 * @author Hélène Collavizza
 */

import back.hoare.HoareTriplets;
import mddapi.apitotem.CSVParaWriter;
import util.jclock.Clock;
import back.net.Net;
import run.NuSMV;
import util.Out;
import util.StdoutReader;

import java.io.*;
import java.util.Iterator;


public class InstanciatedSearchEngine extends AbstractSearchEngine {

    public InstanciatedSearchEngine(Net n, String nuSMVcommand) {
        super(n, nuSMVcommand);
    }

    /////////////////////////////////////////////
    // gestion de la recherche

    // information on search process
    // statistiques affichées toutes les secondes
    public static String stats() {
        String s1 = " [#Checked Models : " + nbSolvedModel + "] ";
        String s2 = "[#Selected  Models : " + nbSelected + "] ";
        return s1 + s2;
    }

    ///////////////////////////////
    // méthodes de recherche de l'interface Search
    public void initSearch(String name) throws Exception {
        // on calcule le 1er modèle
        net.firstParameterization();
    }

    public boolean hasNext() {
        return super.net.nextParameterization();
    }

    @Override
    public void doSearch(String name, boolean csv) throws Exception {
      super.search(name,null,csv,false);
    }

    ////////////////////
    // sauvegarde des résultats

    public void writeInfoModels(Clock c) throws Exception {
        Out.psfln();
        Out.psfln();
        Out.psfln("########### Result of model checking ###########");
        if (Net.hasHoare()) {
            Out.psfln("# Total number of models: " + super.net.NB_PARAMETERIZATIONS.add(HoareTriplets.nbRemovedParam));
            Out.psfln("# Number of models removed using Hoare: " + HoareTriplets.nbRemovedParam);
            Out.psfln("# Remaning models to check: " + super.net.NB_PARAMETERIZATIONS);
        }
        else {
            Out.psfln("# Total number of models: " + super.net.NB_PARAMETERIZATIONS);
        }
        Out.psfln("# Selected models: " + super.OKModels.size());
        Out.psfln("# Computation time: " + c);

    }

    // sauvegarde des résultats
    public void writeSearchInCSV(Clock c, String file) throws Exception {
        super.writeSearchInCSV(c, file);
    }

}


