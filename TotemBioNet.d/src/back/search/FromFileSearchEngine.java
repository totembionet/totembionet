package back.search;

/**
 * This class is used to search for valid models
 * All models are enumerated
 *
 * @author Hélène Collavizza
 */

import back.hoare.HoareTriplets;
import run.Main;
import util.TotemBionetException;
import util.jclock.Clock;
import back.net.Net;
import util.Out;

import java.io.*;
import java.util.Arrays;


public class FromFileSearchEngine extends AbstractSearchEngine {

    // para enumérateur à partir d'un csv
    static FromFileParaEnumerator ffParaEnum;

    public FromFileSearchEngine(Net n, String nuSMVcommand, String path,String type) throws TotemBionetException, IOException {
        super(n, nuSMVcommand);
        int ind = path.lastIndexOf('/');
        String fileName = (ind==-1)?path:path.substring(ind+1);
        ffParaEnum = new FromFileParaEnumerator(type,path,net.getPureGenes());
     }


    /////////////////////////////////////////////
    // gestion de la recherche

    // information on search process
    // statistiques affichées toutes les secondes
    public static String stats() {
        String s1 = " [#Checked Models : " + nbSolvedModel + "] ";
        String s2 = "[#Selected  Models : " + nbSelected + "] ";
        return s1 + s2;
    }

    ///////////////////////////////
    // méthodes de recherche de l'interface Search
    public void initSearch(String name) throws Exception {
        // on calcule le 1er modèle
       ffParaEnum.firstParameterization();
    }

    public boolean hasNext() {
        //return mddParaEnum.nextParameterization();
        return ffParaEnum.nextParameterization();
    }

    @Override
    // resoud un modèle
    public void doSearch(String name, boolean csv) throws Exception {
        if (!ffParaEnum.stop) {
            byte[] model = ffParaEnum.getCurrentModel();
            super.search(name, model, csv, true);
        }
    }

    // ajoute un modèle aux listes de modèles déjà résolus
    @Override
    protected void saveModel(byte[] model, boolean ok) {
        if (ok) {
            if (!ffParaEnum.needCheck()||(ffParaEnum.needCheck() && !contains(model,true)))
                OKModels.add(model);
            if (Out.verb()==2)
                super.addMDDRow(model,ok);
            nbSelected++;
        } else {
            KOModels.add(model);
            nbKO++;
        }
    }

    // pour savoir si le modèle existe déjà (possible à cause des - ou min..max dans .csv
    private boolean contains(byte[] model, boolean ok){
        for (byte[] b : OKModels) {
            if (Arrays.equals(model,b))
                return true;
        }
        return false;
    }

    ////////////////////
    // sauvegarde des résultats
    protected void writeInfoModels(Clock c) throws Exception {
        ffParaEnum.close();
        Out.psfln();
        Out.psfln();
        Out.psfln("########### Result of model checking ###########");
        if (Net.hasHoare()) {
            Out.psfln("# Total number of models: " + super.net.NB_PARAMETERIZATIONS.add(HoareTriplets.nbRemovedParam));
            Out.psfln("# Number of initial models inconsitent with Hoare triple: " + HoareTriplets.nbRemovedParam);

        }
        else {
            Out.psfln("# Total number of models: " + super.net.NB_PARAMETERIZATIONS);
        }
        Out.psfln("# Number of " + Main.fromType() + " models in csv file " + ffParaEnum.nbModels);
        Out.psfln("# Selected models: " + super.OKModels.size());
        Out.psfln("# Computation time: " + c);

    }

    // sauvegarde des résultats
    public void writeSearchInCSV(Clock c, String file) throws Exception {
        super.writeSearchInCSV(c, file+"-from");
    }

}
