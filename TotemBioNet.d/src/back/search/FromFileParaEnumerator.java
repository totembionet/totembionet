package back.search;

import back.net.Gene;
import back.net.Para;
import util.TotemBionetException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;


public class FromFileParaEnumerator implements ParaEnumerator {
    // NOTA : il faut énumérer sur les gènes avec nextPara et firstPara pour avoir le bon énumérateur (Snoussi ou non)
// mais on peut modifier directement les para pour changer l'énumération

    // les gènes qui contiennent les para et leur énumérateur (Snoussi ou non)
    private ArrayList<Gene> genes;
    private HashMap<String, Gene> mapGenes;

    // association (nom para,objet Para) relié à un gène
    // utile pour changer la valeur du para en fonction
    private HashMap<String, Para> mapParas;
    // associates to each para name its column index into the csv file
    private HashMap<Integer,Para> paraIndex;
    // associates to each para its domain in the .smb file
    private HashMap<Para,int[]> paraInitDomain;

    // le fichier
    private BufferedReader br;
    // le modèle
    private int[] model;
    // la ligne en cours de traitement
    String[] row;
    // to avoid use of Exception. Otherwise, IOexception should be thrown for each enumerator
    boolean stop;
    // OK or KO according to models we want for enumerating
    private String valid;
    // number of enumerated models
    BigDecimal nbModels;
    // true if one row contains - or min..max => en énumérant, on peut avoir plusieurs fois le même modèle
    // exemple : 0 1 0 0 1
    //           0 - 0 0 1
    // on a 2 fois 0 1 0 0 1 => ne sera pas ajouté aux OK ou KO modèles
    private static boolean needCheck =false;

    // to know the length of row that contains useful information
    //    => avoid error explanation and model id
    private int usefulLength =0;

    // to know the number of effective para
    private int nbEffectiveParas;

    public FromFileParaEnumerator(String valid, String path, ArrayList<Gene> genes) throws IOException, TotemBionetException {
        this.valid=valid;
        this.nbModels = new BigDecimal(0);
        this.br = new BufferedReader(new FileReader(path));
        this.genes = (ArrayList<Gene>) genes.clone();
        initParaDomains( genes);
        // initialize link between gene names to genes
        initMapGene(genes);
        // initialize link between para names extracted from .csv and Para associated with the genes
        // read the first 2 lines (2d line to read parameter domains)
        initMapParas();
        // number of effective parameters
        nbEffectiveParas =0;
        for (Gene g : genes){
            nbEffectiveParas +=g.paraSize();
        }
        // read one line
        stop = !nextRow();
    }

    public void close() throws IOException {
        br.close();
    }

    // methods to initialize maps from genes
    ///////////////////////////////////////////////////

    // build the association from Para object to their domains
    private void initParaDomains(ArrayList<Gene> genes) {
        paraInitDomain = new HashMap<Para, int[]>();
        for (Gene g : genes) {
            for (Para p : g.getParas()) {
                int mini = p.minLevel();
                int maxi = p.maxLevel();
                int[] dom = {mini,maxi};
                paraInitDomain.put(p,dom);
            }
        }
    }

    // maps gene names to Gene objects
    private void initMapGene(ArrayList<Gene> genes) {
        mapGenes = new HashMap<String, Gene>();
        for (Gene g : genes) {
            mapGenes.put(g.toString(), g);
        }
    }


    // methods to read the csv file and modify paras values
    // associated with the genes
    ///////////////////////////////////////////////////

    /**
     * Read the csv header that contains variable names and the second line that contains the parameter
     * domains and initialize the map of parameters
     */
    private void initMapParas() throws IOException, TotemBionetException {
        mapParas = new HashMap<String, Para>();
        paraIndex = new HashMap<Integer, Para>();
        String line1 = br.readLine();
        String[] rows1 = line1.split(";");
        usefulLength = rows1.length;
        String line2 = br.readLine();
        String[] rows2 = line2.split(";");
        for (int i = 2; i < rows1.length; i++) {
            // si c'est Error explanation ou indent
            if (rows1[i].startsWith("K_")) {
                String gName = getGeneName(rows1[i]);
                Gene g = mapGenes.get(gName);
                if (g == null)
                    throw new TotemBionetException("Gene " + gName + " doesn't exist (from ident " + rows1[i] + ") in csv file");

                Para p = g.getParaByName(rows1[i]);
                mapParas.put(rows1[i], p);
                paraIndex.put(i, p);
                if (p == null)
                    System.err.println("***   WARNING Para " + rows1[i] + " doesn't exist or is not effective. It will be ignored.");
                else {
                    // set the para domain
                    String[] parts = rows2[i].split("\\.\\.");
                    int maxi = Integer.parseInt(parts[1]);
                    int mini = Integer.parseInt(parts[0]);
                    int[] dom = paraInitDomain.get(p);
                    // the domain in the .smb takes precedence on the domain in .csv
                    if ((dom[0] > mini) || dom[1] < maxi) {
                        String smb = dom[0]+ ".." + dom[1];
                        System.err.println("***   WARNING Incompatible domains for Para " + p + ": " + smb + " in .smb / " +
                                 rows2[i] + " in .csv. " + smb + " is used");
//                        throw new TotemBionetException("Parameter bounds " + rows2[i] + " are impossible for " + p);
                    }
                    else
                        p.setMinMax(mini, maxi);
                }
            }
            else
                usefulLength = usefulLength-1;
        }
    }

    // build the gene name from a string which is the name of a parameter
    private String getGeneName(String s) {
        int tiret = s.indexOf("_");
        int dot = s.indexOf(":");
        if (dot == -1)
            return s.substring(tiret + 1);
        return s.substring(tiret + 1, dot);
    }

    // read the next row
    private boolean nextRow() {
        try {
            String line = br.readLine();
            if (line == null)
                return false;
            // to avoid problem with error explanation
            int crochet = line.indexOf('[');
            if (crochet != -1) {
                line = line.substring(0, crochet - 1);
            }
            row = line.split(";");
            // keep if it is valid
            if (row[1].equals(valid))
                return true;
            // look for next one if not valid
            return nextRow();
        }catch(IOException e) {
            e.printStackTrace();
            stop=true;
        }
        return false;
    }

    // set the values of the parameters from one file row
    private void setRowValue(){
        // max to usefulLength to avoid error explanation
        for (int k = 2; k < usefulLength; k++) {
            String str = row[k];
            Para p = paraIndex.get(k);
            if (p!=null) {
//            System.out.println("index " + k + p);
                // all the initial domain
                if (str.equals("-")) {
                    needCheck = true;
                    int[] dom = paraInitDomain.get(p);
                    p.setMinMax(dom[0], dom[1]);
                } else {
                    // the domain given into the csv file if it is correct
                    if (str.contains("..")) {
                        needCheck = true;
                        ;
                        String[] parts = str.split("\\.\\.");
                        int maxi = Integer.parseInt(parts[1]);
                        int mini = Integer.parseInt(parts[0]);
                        int[] dom = paraInitDomain.get(p);
                        try {
                            if ((dom[0] > mini) || dom[1] < maxi)
                                throw new TotemBionetException(str + " is out of bounds for parameter " + p);
                        } catch (TotemBionetException e) {
                            e.printStackTrace();
                            stop = true;
                        }
                        p.setMinMax(mini, maxi);
                    } else {
                        // the value given into the csv file
                        int v = Integer.parseInt(str);
                        p.setMinMax(v, v);
                    }
                }
            }
        }
    }

    // inherited methods from AbstractSearchEngine
    //////////////////////////////////////////////

    @Override
    public void firstParameterization()  {
        if (!stop) {
            nbModels = nbModels.add(BigDecimal.valueOf(1));
            // set values of parameters according to the current row
            setRowValue();
            // do the 1st parameterization for all genes
            for (Gene g : genes)
                g.firstParameterization();
        }
    }

    @Override
    public boolean nextParameterization() {
        if (stop)
            return false;
        // do the next parameterization for all genes
        for (Gene g : genes)
            if (g.nextParameterization()) {
                //printAllCurrentIntervals();
                nbModels=nbModels.add(BigDecimal.valueOf(1));
                return true;
            }
        // when all genes have reached their last parameterization for this row
        // check whether there is another row
        if (nextRow()){
            firstParameterization();
            return true;
        }
        return false;
    }

    // build the current model (i.e. value of parameters) as a byte array
    public byte[] getCurrentModel() {
        byte[] para = new byte[nbEffectiveParas + 1];
        para[0] = 0;
        int k = 1;
        for (int i = 0; i < genes.size(); i++) {
            Gene g = genes.get(i);
            for (Para p : g.getParas()) {
                para[k] = (byte) p.currentMin();
                k++;
            }
        }
//        System.out.println("paras current " + Arrays.toString(para));
        return para;
    }

    // true if there are abstract values in the csv file to don't add twice the same model
    public boolean needCheck() {
        return needCheck;
    }

    public String toString() {
        String s = "";
        for (Para p : mapParas.values()) {
            s += p.getSMBname() + ":" + p.currentMin() + "\n";
        }
        return s;
    }


}

