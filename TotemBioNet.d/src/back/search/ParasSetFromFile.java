package back.search;

import java.util.HashSet;

/**
 * class to store a set of parameterizations which provide from
 * files where previous experiments have been stored
 *
 * @author Hélène Collavizza
 */
public class ParasSetFromFile {

    private HashSet<byte[]> paraSet;

    public ParasSetFromFile() {
        paraSet = new HashSet<byte[]>();
    }

    public void addParameterization(String fileName) {
        // ajoute les param issues de fileName ie les OK model
    }

    public void removeParameterization(String fileName) {
        // enlève les param issues de fileName ie les KO model
    }

    // TODO : on a besoin du Net pour connaitre le nombre de paras => le nb de colonnes du csv
    private void addParameterization(String fileName, boolean ok){
        // lit le fichier fileName ligne à ligne
        // si ok, ajoute les lignes étiquetées OK à paraSet
        // si !ok, supprime les lignes étiquetées KO à paraSet
        // TODO : on doit commencer par des OK => le constructeur doit déjà prendre
        // un fileName ou bien on part de toutes les paramétrisations
    }
}
