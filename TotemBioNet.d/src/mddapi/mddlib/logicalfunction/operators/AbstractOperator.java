package mddapi.mddlib.logicalfunction.operators;

import mddapi.mddlib.MDDManager;
import mddapi.mddlib.logicalfunction.FunctionNode;
import mddapi.mddlib.logicalfunction.FunctionParser;


/**
 * Common methods for internal nodes (operators).
 *
 * @author Fabrice Lopez: initial implementation
 * @author Aurelien Naldi: adaptation
 */
public abstract class AbstractOperator implements FunctionNode {
  protected String returnClassName;
  protected FunctionParser parser;

  public AbstractOperator() {
    super();
  }
  
  @Override
  public String toString() {
	  return toString(false);
  }
  
  @Override
  public boolean isLeaf() {
    return false;
  }

  public abstract String getSymbol();
  public abstract int getNbArgs();
  public abstract FunctionNode[] getArgs();
}
