package mddapi.apitotem;


import java.util.ArrayList;
import java.util.HashMap;

public class Test {

    public static void main(String[] args ) {
        testNaldi();
        //testAllParam();
    }

//    private static void intersect() {
//        MDDFacade.allIntersection("examples/intersection/csv_files");
//    }

//    private static void testAllParam() {
//        HashMap<String , Integer> solution = new HashMap<>();
//        HashMap<String, Integer> var_range = new HashMap<>();
//        int max = 100;
//        for (int i=0;i<max;i++) {
//            String name = "k" + i;
//            solution.put(name, 0);
//            var_range.put(name,3);
//        }
//        String[] var_to_change = new String[] {"k0","k2", "k3","k8"};
//        MDDParaSet sol = MDDSolutionBuilder.createMDDSolution(solution, var_range, var_to_change);
//        System.out.println(sol.dumpMDD());
//                mddapi.mddlib.PathSearcher ps = new mddapi.mddlib.PathSearcher(sol.mddManager,1);
//        ps.setNode(sol.rootNode);
//        int i=1;
//        for (int p : ps) {
//            // nota: il y a un itérateur qui renvoie un int mais
//            //       bug : il renvoie toujours 1 => on ne peut pas s'en servir
//            //       pour numéroter les chemins donc il faut utiliser aussi un i
//            int[] path = ps.getPath();
//            System.out.print("path " + i + " : ");
//            for (int x : path)
//                System.out.print(x + " ");
//            System.out.println();
//            i++;
//        }
//    }

    private static void testUtilitairesCyril() {
        class Parameter {
            String name;
            int min, max;

            Parameter(String n, int mi, int ma){
                name=n;
                min=mi;
                max=ma;
            }

            public String toString() {
                return name;
            }
        }

        mddapi.mddlib.MDDVariableFactory vbuilder= new mddapi.mddlib.MDDVariableFactory();
        vbuilder.add(new Parameter("K1",0,4),(byte)5);
        vbuilder.add(new Parameter("K2",1,2),(byte)2);
        vbuilder.add(new Parameter("K3",0,1),(byte)2);
//        vbuilder.add(new Parameter("K4",0,2),(byte)3);


        mddapi.mddlib.MDDManager mddManager = mddapi.mddlib.MDDManagerFactory.getManager( vbuilder, 1);
        System.out.println("mdd manager " + mddManager.dumpMDD(1));

        ArrayList<int[]> valList = new ArrayList<>();
        int[] val1 = {4,1,0};
        valList.add(val1);
        int[] val2 = {0,1,0};
        valList.add(val2);

        MDDBuilder mdd = new MDDBuilder();
        MDDParaSet m = mdd.getMDD(valList,vbuilder,"OK");
        System.out.println("mdd : " + m.dumpMDD());
        System.out.println("# feuilles : " + m.mddManager.getLeafCount());

    }

    private static void testNaldi() {
        mddapi.mddlib.MDDVariableFactory varFactory = new mddapi.mddlib.MDDVariableFactory();
		for (int i = 0; i < 5; i++) {
			varFactory.add("var" + i, (byte)3);
		}
		mddapi.mddlib.MDDManager ddmanager = mddapi.mddlib.MDDManagerFactory.getManager( varFactory, 10);
		mddapi.mddlib.MDDVariable[] variables = ddmanager.getAllVariables();

		// créé le noeud où v4 vaut 2
		int n1 = variables[4].getNode(new int[]{0, 0, 1});
		// créé le noeud où v4 vaut 0
		int n2 = variables[4].getNode(new int[]{1, 0, 0});
		// créé le noeud tq v2 va vers v4 (val 2) quand il vaut 1 et va vers v4 (val 0)
        // quand il vaut 2 et pas de fils quand il vaut 0.
		int n3 = variables[2].getNode(new int[]{1, n1, n2});
        System.out.println("mdd n1: " + ddmanager.dumpMDD(n1));
        // mdd n1: (4,0,0,1)
        System.out.println("mdd n2: " + ddmanager.dumpMDD(n2));
        // mdd n2: (4,1,0,0)
        System.out.println("mdd n3: " + ddmanager.dumpMDD(n3));
        // mdd n3: (2,1,(4,0,0,1),(4,1,0,0))

        System.out.println("nodes : " + n1 + " ," + n2 + " , " +n3);
        //nodes : 10 ,15 , 20

        boolean[] affect = ddmanager.collectDecisionVariables(n3);
        for (boolean b : affect)
            System.out.print(b + " ");
        System.out.println();
        // false false true false true
        // car le mdd ne dépend que de v2 et de v4

        byte[] val ={2,0,1,0,2};
        System.out.println(" reached : " + ddmanager.reach(n3,val));
        //val ={0,0,0,0,0}; reached : 1
        // val ={0,0,1,0,0}; reached : 0
        // val ={2,0,1,0,0};reached : 0
        // val ={2,0,0,0,0};reached : 1
        // val ={2,0,2,0,2};reached : 1
        // val ={2,0,1,0,1};reached : 0
        // val ={2,0,1,0,2};reached : 1
        // en partant d'un noeud, reach renvoie 1 si on peut atteindre
        // une feuille en suivant les valeurs passées dans val pour les différentes variables
        // renvoie 0 si pas de chemin
        // si on met n'importe quoi pour une variable qui n'est pas une variable de décision,
        // alors ça ne perturbe pas

        // les chemins
        mddapi.mddlib.PathSearcher ps = new mddapi.mddlib.PathSearcher(ddmanager);
        ps.setNode(n3);
        int i=1;
        for (int p : ps) {
            // nota: il y a un itérateur qui renvoie un int mais
            //       bug : il renvoie toujours 1 => on ne peut pas s'en servir
            //       pour numéroter les chemins donc il faut utiliser aussi un i
            int[] path = ps.getPath();
            System.out.print("path " + i + " : ");
            for (int x : path)
                System.out.print(x + " ");
            System.out.println();
            i++;
        }

        // avec new PathSearcher(ddmanager,1);
        //path 1 : -1 -1 0 -1 -1
//        path 2 : -1 -1 1 -1 2
//        path 3 : -1 -1 2 -1 0
       // avec new PathSearcher(ddmanager,0);
//path 1 : -1 -1 1 -1 0
//path 2 : -1 -1 1 -1 1
//path 3 : -1 -1 2 -1 1
//path 4 : -1 -1 2 -1 2
       // avec new PathSearcher(ddmanager) on a l'union des deux

    }
}
