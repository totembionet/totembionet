package mddapi.apitotem;

import mddapi.mddlib.MDDManager;
import mddapi.mddlib.MDDManagerFactory;
import mddapi.mddlib.MDDVariable;
import mddapi.mddlib.MDDVariableFactory;
import mddapi.mddlib.operators.MDDBaseOperators;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;


/**
 *
 * Class to apply basic combinators to MDDs
 *
 * @author Cyril Guillot
 * Polytech Sophia - SI4
 *
 * @author Hélène Collavizza
 */


public class MDDCombiner {

    /**
     *
     * @param sets a set of MDD to combine
     * @param op the operation (intersection or union)
     * @return a MDD
     *
     * NOTA: set of variables and variables domains must be the same in each MDD
     */
    static MDDParaSet createMDD(ArrayList<MDDParaSet> sets, MDDFacade.Operation op) {
        if (sets.size() == 0) {
            return null;
        }
        MDDVariableFactory vbuilder = sets.get(0).vbuilder;
        // to have the same manager
        MDDManager newMDDManager = MDDManagerFactory.getManager(vbuilder, 2);
        int[] root_nodes = buildRowsToCombine(sets,newMDDManager);
        int root = combine(newMDDManager, root_nodes,op);
        return new MDDParaSet(op.name(), sets.get(0).type,sets.get(0).variablesDomains,newMDDManager, vbuilder, root);
    }


    /**
     *
     * @param sets
     * @param newMDDManager
     * @return the roots of the MDD unified in the same MDDManager
     *
     * TODO: dump + parseDump trop coûteux ? Faire le op l'un après l'autre (i.e. fichier résultat op fichier courant)?
     */
    private static int[] buildRowsToCombine(ArrayList<MDDParaSet> sets, MDDManager newMDDManager) {
        int[] root_nodes = new int[sets.size()];
        for (int i = 0; i < sets.size(); i++) {
            // to rebuild the MDD inside the same manager
            // without this line, Exception because number of nodes are not coherent
            // (same node numbers in different MDDs)
            String mdd = sets.get(i).dumpMDD();
            try {
                root_nodes[i] = newMDDManager.parseDump(mdd);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return root_nodes;
    }

    /**
     *
      * @param newMDDManager: the MDDManager that contains the nodes of all MDDs
     * @param root_nodes
     * @param op
     * @return the MDD that contains the op of all MDD in root_nodes
     */
    private static int combine(MDDManager newMDDManager,int[] root_nodes,MDDFacade.Operation op){
        switch (op){
            case intersection:
                return MDDBaseOperators.AND.combine(newMDDManager, root_nodes);
            case union:
                return MDDBaseOperators.OR.combine(newMDDManager, root_nodes);
            default:
                throw new IllegalStateException("Unexpected value: " + op);
        }
    }


}
