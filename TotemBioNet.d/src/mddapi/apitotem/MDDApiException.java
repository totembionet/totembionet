package mddapi.apitotem;

/**
 *
 * @author Cyril Guillot
 * Polytech Sophia - SI4
 */


public class MDDApiException extends Exception {

    public MDDApiException(String m) {
        super("[MDDApi error] " + m);
    }
}
