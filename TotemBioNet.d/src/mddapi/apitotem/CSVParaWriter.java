package mddapi.apitotem;


import back.net.Gene;
import back.net.Net;
import back.net.Para;
import mddapi.mddlib.MDDVariableFactory;
import mddapi.mddlib.PathSearcher;
import util.Out;
import util.TotemBionetException;

import java.io.*;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * Class to write set of datato csv files
 *
 * @author Cyril Guillot
 * Polytech Sophia - SI4
 *
 * @author Hélène Collavizza
 */

public class CSVParaWriter {

    // where to write the MDD
    private BufferedWriter writer;
    // variables that are not involved in the MDD (they have been abstracted to "-"
    // because the whole domain is possible
    private HashSet<String> unusedVar;

    public CSVParaWriter(String path, HashSet<String> unusedVar) throws IOException {
        writer = new BufferedWriter(new FileWriter(new File(path)));
        this.unusedVar = unusedVar;
     }

    /**
     * Write a MDD into a csv file
     * @param set: a set of parameters in an MDD
     * @throws IOException
     */
    public BigDecimal writeCSV(MDDParaSet set) throws IOException,TotemBionetException {
        String head = getCSVHeader(set.vbuilder);
        String domains = getCSVDomains(set);
        writer.write(head);
        writer.write(domains);
        BigDecimal nbModel = writePaths(set);
        writer.close();
        return nbModel;
    }

    /**
     * Write all the paths of the MDD (i.e. all possible values)
     * @param set
     * @throws IOException
     * @return
     */
    private BigDecimal writePaths(MDDParaSet set) throws IOException, TotemBionetException {
        String type = set.type;
        PathSearcher ps = set.getPathSearcher();
                //new PathSearcher(set.mddManager, 1);
        //ps.setNode(set.rootNode);
        int i = 0;
        // count the total number of models
        BigDecimal nbTotalModele = BigDecimal.valueOf(0);
        for (int p : ps) {
            BigDecimal nbModele = BigDecimal.valueOf(1);
            int[] path = ps.getPath();
            // model identifier
            String identModel = "#";
            writer.write(i + ";");
            writer.write(type + ";");
            for (int j = 0; j < path.length; j++) {
                String varName = set.vbuilder.get(j).toString();
                if (!unusedVar.contains(varName)) {
                    int[] domain = set.variablesDomains.get(varName);
                    if (path[j] == -1) {
                        identModel+=".";
                        BigDecimal size = BigDecimal.valueOf(domain[2] - domain[1] + 1);
                        nbModele = nbModele.add(nbModele.multiply(size));
                        writer.write(domain[1] + ".." + domain[2]);
                    } else {
                        // here one checks if the value is in the initial domain of the variable
//                        System.out.println(varName + "dom " + domain[0] + " " +  domain[1] + " " + domain[2]);
                        if (path[j] > domain[2] | path[j] < domain[1])
                            throw new TotemBionetException("Value " + path[j] + " of " + varName + " in MDD is out of bounds");
                        writer.write(path[j] + "");
                        identModel+=path[j];
                    }
                    writer.write(';');
                }
            }
            i++;
            // if verbose mode, write the ident of the model
            if (Out.verb()==3)
                writer.write(identModel + ";");
            writer.write('\n');
            nbTotalModele = nbTotalModele.add(nbModele);
        }
        return nbTotalModele;
    }

    /**
     * Write the first line according Totem syntax
     * @param vbuilder
     * @return
     */
    private String getCSVHeader(MDDVariableFactory vbuilder) {
        String header ="";
        header+= "n;";
        header+= "valid;";
        for (int i = 0; i < vbuilder.size(); i++) {
            String n = vbuilder.get(i).toString();
            if (!unusedVar.contains(n))
                header+= n+";";
        }
        if (Out.verb()==3)
            header +="Model ident;";
        return header+"\n";
    }

    /**
     * Write the second line with domains
     * @param mdd
     * @return
     */
    private String getCSVDomains(MDDParaSet mdd) {
        MDDVariableFactory vbuilder = mdd.vbuilder;
        String header =";;";
        for (int i = 0; i < vbuilder.size(); i++) {
            String name = vbuilder.get(i).toString();
            if (!unusedVar.contains(name)) {
                int[] domain = mdd.variablesDomains.get(name);
                header += domain[1] + ".." + domain[2] + ";";
            }
        }
        return header+"\n";
    }
}
