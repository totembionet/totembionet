package mddapi.apitotem;

import mddapi.mddlib.MDDManager;
import mddapi.mddlib.MDDVariableFactory;
import mddapi.mddlib.PathSearcher;
import mddapi.mddlib.operators.MDDBaseOperators;
import util.TotemBionetException;

import java.text.ParseException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Class to represent parameter sets as MDDs
 *
 * @author Hélène Collavizza
 */
public class MDDParaSet {

    private MDDManager mddManager;
    int rootNode;
    int oldRoot;
    private String name;
    MDDVariableFactory vbuilder;
    HashMap<String, int[]> variablesDomains;
    String type;
    private MDDBuilder mddb;

    // used by MDDBuilder for combination operations
    public MDDParaSet(String name, String type, HashMap<String, int[]> variablesDomains, MDDManager mddManager, MDDVariableFactory vbuilder, int rootNode) {
        this.name = name;
        this.type = type;
        this.variablesDomains = variablesDomains;
        this.mddManager = mddManager;
        this.rootNode = rootNode;
        this.vbuilder = vbuilder;
    }

    // used by AbstractSearchEngine when creating an empty MDD
    public MDDParaSet(String name, String type, MDDBuilder mddb, int rootNode) {
        this(name, type, mddb.csvVarToIndexAndDomain, mddb.mddManager, mddb.vbuilder, rootNode);
        this.mddb = mddb;
    }

    /**
     * The path searcher
     *
     * @return
     */
    public PathSearcher getPathSearcher() {
        PathSearcher ps = new PathSearcher(this.mddManager, 1);
        ps.setNode(rootNode);
        return ps;
    }

    /**
     * String representation that can be used to rebuild the MDD with "parseDump"
     *
     * @return
     */
    public String dumpMDD() {
        return mddManager.dumpMDD(rootNode);
    }

    // don't car variables
    public HashSet<String> unusedVariables() {
        return mddb.unusedVariables();
    }

    // true if the MDD is empty
    public boolean isEmpty() {
        PathSearcher ps = new PathSearcher(this.mddManager, 1);
        ps.setNode(rootNode);
        return ps.countPaths() == 0;
    }

    // to add a first row from a OK model (used by AbstractSearchEngine)
    private void addFirstRow(int[] row) throws TotemBionetException {
        rootNode = mddb.addPathFromRow(row, mddManager.getAllVariables());
    }

    // to add a row from a OK model (used by AbstractSearchEngine)
    // NOTA : do the combination as in MDDBuilder.addAllPathsFromRows
    public void addRow(int[] row) throws TotemBionetException {
        //System.out.println("row " + Arrays.toString(row));
        if (rootNode == -1)
            addFirstRow(row);
        else {
            int[] roots = new int[2];
            String mdd = this.dumpMDD();
            try {
                roots[0] = mddManager.parseDump(mdd);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            roots[1] = mddb.addPathFromRow(row, mddManager.getAllVariables());
            rootNode = MDDBaseOperators.OR.combine(mddManager, roots);
        }
    }

    ////////////////////////////////////////////////
    // used for -compare option
    // need to apply not inside this class to have the right MDDManager
    void makeNot() {
        oldRoot = rootNode;
        rootNode = mddManager.not(rootNode);
    }

    void unMakeNot() {
        rootNode = oldRoot;
    }

    /**
     *
     * @return number of paths i.e. number of lines in csv file
     */
    public int nbPaths() {
        PathSearcher ps = new PathSearcher(this.mddManager, 1);
        ps.setNode(rootNode);
        return ps.countPaths() ;
    }

    public String getName() {
        return name;
    }

    public int getRootNode() {
        return rootNode;
    }

    public Set<String> getVariables() {
        return variablesDomains.keySet();
    }

    @Override
    public String toString() {
        PathSearcher ps = new PathSearcher(this.mddManager, 1);
        ps.setNode(rootNode);
        String res=name + "variables : " + variablesDomains.keySet();
        res+="MDD has "  + ps.countPaths() + " paths\n";
        int k= 0;
        for (int p : ps) {
            int[] path = ps.getPath();
            String str = "Path " + k + "::: ";
            for (int i = 0; i < path.length; i++) {
                str += vbuilder.get(i) + " : " + path[i] + ", ";
            }
            k++;
            res+=str+"\n";
        }
        return res;
    }
}