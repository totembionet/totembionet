package mddapi.apitotem;

import mddapi.mddlib.*;
import mddapi.mddlib.operators.MDDBaseOperators;
import util.Out;
import util.TotemBionetException;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Scanner;

/**
 * Class to launch intersection or union of MDDs
 *
 * @author Cyril Guillot
 * Polytech Sophia - SI4
 *
 * @author Hélène Collavizza
 */

public class MDDFacade {

    // the firts MDD builder
    // required to compare variable information into the whole set of files
    static MDDBuilder firstMddBuilder;

    // to stop if there is a file that contains no model of the right type
    static boolean emptyIntersect = false;

    static long timeToBuild = 0;
    static long timeToIntersect = 0;


    public enum Operation {
        intersection,
        union
    }

    // read the first csv file and get the first MDDBuilder
    // return a MDParaSet to add to the combination
    private static MDDParaSet manageFirstFile(File file, String op, String type) throws TotemBionetException {
        MDDLogger.mddInfo("Reading file: " + file);
        firstMddBuilder = new MDDBuilder(file.getName());
        MDDParaSet ps = firstMddBuilder.createFromCSV(file.toString(), type);
        timeToBuild += firstMddBuilder.timeToBuild;
        if (ps == null && op.equals("intersection")) {
            emptyIntersect = true;
        }
        return ps;
    }

    // read a csv file and check if it is conform to the first csv file. If yes,
    // return the associated MDParaSet to add to the combination
    private static MDDParaSet manageFile(File file, String op, String type) throws TotemBionetException {
        MDDLogger.mddInfo("Reading file: " + file);
        MDDBuilder m = new MDDBuilder(file.getName(), firstMddBuilder.vbuilder, firstMddBuilder.containsAValue, firstMddBuilder.csvVarToIndexAndDomain);
        MDDParaSet ps = m.createFromCSV(file.toString(), type);
        timeToBuild += m.timeToBuild;
        if (ps == null && op.equals("intersection")) {
            emptyIntersect = true;
        }
        return ps;
    }

    // main method to combine a set of files which are in directory path
    public static void combineAll(String path, Operation op, String type) throws TotemBionetException, IOException {

        // initializations
        String operation = op.name();
        String fileName = operation + type;
        String output = path + fileName + ".csv";
        ArrayList<MDDParaSet> parameterSets = new ArrayList<>();

        // reading the directory
        MDDLogger.mddInfo("\nCreating MDD from files ...\n");
        File folder = new File(path);
        File[] files = folder.listFiles();
        if (files == null)
            throw new TotemBionetException("Path " + path + " doesn't exist");

        // manage the 1st file to get its vbuilder and the variable domains
        // need to skip files which are not .csv file file named intersection.csv or union.csv
        // which is the output file
        int i = 0;
        while (i < files.length && isIgnored(path, files[i].toString())) {
            if (files[i].toString().equals(output))
                //output = readOutputName(output,path);
                System.err.println("   ***   WARNING: File " + fileName + " exists, it will be erased");
            i++;
        }
        if (i == files.length)
            throw new TotemBionetException("There are no .csv file in " + path);
        MDDParaSet ps = manageFirstFile(files[i], operation, type);
        if (ps != null) {
            parameterSets.add(ps);
        }
        i++;
        // manage other files
        while (!emptyIntersect && i < files.length) {
            if (isIgnored(path, files[i].toString())) {
                if (files[i].toString().equals(output))
                    //output = readOutputName(output,path);
                    System.err.println("   ***   WARNING: File " + fileName + " exists, it will be erased");
            } else if (files[i].toString().endsWith(".csv")) {
                ps = manageFile(files[i], operation, type);
                if (ps != null)
                    parameterSets.add(ps);
            }
            i++;
        }

        // combine the MDDParaSet that have been built
        combine(parameterSets, output, operation, type);
    }

    // combine the MDDParaSet that have been built
    private static void combine(ArrayList<MDDParaSet> parameterSets, String output, String operation, String type) throws IOException, TotemBionetException {
        // pas de modèles du bon type trouvé
        if (emptyIntersect)
            MDDLogger.mddInfo("   No model for type " + type + " has been found in one of the files. The " + operation + " is empty.");
        else if (parameterSets.size() == 0)
            MDDLogger.mddInfo("   No model for type " + type + " has been found. The " + operation + " is empty.");
            // models have been found, do the combination
        else {
            MDDLogger.mddInfo("Creating " + operation + " of " + parameterSets.size() + " MDDs for " + type + " models ...");
            long t1 = System.currentTimeMillis();
            MDDParaSet combined = MDDCombiner.createMDD(parameterSets, Operation.valueOf(operation));
            long t2 = System.currentTimeMillis();
            timeToIntersect = t2 - t1;
            System.out.println("\nTime to build all MDDs " + timeToBuild + "ms");
            System.out.println("Time to make intersection " + timeToIntersect + "ms");
            System.out.println("!!! Total time for intersection " + (timeToBuild + timeToIntersect) + "ms\n");
            if (!combined.isEmpty()) {
                MDDLogger.mddInfo("Writing result of " + operation);
                CSVParaWriter pw = new CSVParaWriter(output, firstMddBuilder.unusedVariables());
                BigDecimal nbTotal = pw.writeCSV(combined);
//                    if (Out.verb()==2)
//                        pw.writeDetailedCSV(combined,net);
                MDDLogger.mddInfo("\n  The " + operation + " has been written into " + output);
                MDDLogger.mddInfo("  The " + operation + " contains " + combined.nbPaths() + " lines and "
                        + nbTotal + " no Snoussi models.\n");
            } else {
                System.out.println("  The " + operation + " is empty.");
            }
        }
    }

    // true if testedFile must be ignored: .csv file
    private static boolean isIgnored(String path, String testedFile) {
        return isOutput(path, testedFile) | (!testedFile.endsWith(".csv"));
    }

    // true if testedFile must be ignored: output of a previous intersection or union
    private static boolean isOutput(String path, String testedFile) {
        String inter = path + "intersection";
        String union = path + "union";
        return testedFile.startsWith(inter) || testedFile.startsWith(union);
    }

    // to compare 2 csv files which are in Totem format. Build the MDD : MDD_f1 and not MDD_f2. If this MDD is empty,
    // files contains the same values
    // if files differ, the difference is written in two .csv.
    public static void compare(String path, String fileName1, String fileName2) throws TotemBionetException, IOException {
        // manage files
        if (!fileName1.endsWith(".csv") | !fileName2.endsWith(".csv"))
            throw new TotemBionetException("-compare can be used only for .csv files");
        String rootFile1 = fileName1.substring(0, fileName1.length() - 4);
        String rootFile2 = fileName2.substring(0, fileName2.length() - 4);
        // build the MDDParaSet of the 2 files
        MDDLogger.mddInfo("\n-compare : creating MDD from OK models in files ...\n");
        MDDParaSet first;
        MDDParaSet second;
        try {
            first = manageFirstFile(new File(path + "/" + fileName1), "compare", "OK");
            second = manageFile(new File(path + "/" + fileName2), "compare", "OK");
        } catch (TotemBionetException e) {
            System.out.println("\n  " + fileName1 + " and " + fileName2 + " differ.");
            System.out.println(e.getMessage());
            return;
        }
        // make not MDD_f2
        second.makeNot();
        // build the ArrayList for combining the MDDs
        ArrayList<MDDParaSet> set = new ArrayList<>();
        set.add(first);
        set.add(second);

        // NOTA: because of not used variables, need to make (f1 and not f2) and also (f2 and not f1)
        //       (f1 and not f2) can be empty while it is not when x is not used in f1 and used in f2

        // (f1 and not f2)
        MDDParaSet result1 = MDDCombiner.createMDD(set, Operation.valueOf("intersection"));
        first.makeNot();
        second.unMakeNot();
        // (f2 and not f1)
        MDDParaSet result2 = MDDCombiner.createMDD(set, Operation.valueOf("intersection"));

        // analyze and write results
        if (result1.isEmpty() && result2.isEmpty()) {
            System.out.println("   " + fileName1 + " and " + fileName2 + " contain the same OK models.");
        }
        else {
            if (!result1.isEmpty()) {
                // write values which are in f1 and not in f2
                String output = path + "/" + rootFile1 + "-notIn-" + rootFile2 + ".csv";
                CSVParaWriter pw = new CSVParaWriter(output, firstMddBuilder.unusedVariables());
                BigDecimal nbTotal = pw.writeCSV(result1);
                System.out.println("\n   " + fileName1 + " and " + fileName2 + " differ. There are " + nbTotal + " OK models in "
                        + fileName1 + " which are not in " + fileName2 + ". These models have been written in " + output + ".");
            }
            if (!result2.isEmpty()) {
                // write values which are in f2 and not in f1
                String output = path + "/" + rootFile2 + "-notIn-" + rootFile1 + ".csv";
                CSVParaWriter pw = new CSVParaWriter(output, firstMddBuilder.unusedVariables());
                BigDecimal nbTotal = pw.writeCSV(result2);
                System.out.println("   There are " + nbTotal + " OK models in "
                        + fileName2 + " which are not in " + fileName1 + ". These models have been written in " + output + ".");
            }
        }
    }



//    private static String readOutputName(String output, String path) {
//        String response = "";
//        Scanner scanner = new Scanner(System.in);
//        System.out.print("   WARNING: File " + output + " already exists. Do you want to overwrite it ? y/n : ");
//        response = scanner.next();
//        if (response.equals("n")) {
//            System.out.print("Please give a file name <name>");
//            System.out.print("    The file will be generated in " + path + "<name>.csv ");
//            response = scanner.next();
//            return path + response + ".csv";
//        }
//        scanner.close();
//        return output;
//    }

}

