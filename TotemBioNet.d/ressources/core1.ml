(***
Implémentation du calcul de plus faible précondition à l'aide de logique de Hoare
sur des réseaux de régulation biologique.

@author Maxime Folschette

Référence :
  G. Bernot, J.-P. Comet, Z. Khalis, A. Richard, O. Roux,
  A genetically modified Hoare logic,
  Theoretical Computer Science,
  2018.
  ISSN: 0304-3975
  DOI: https://doi.org/10.1016/j.tcs.2018.02.003

Cf. documentation du fichier README

Principe :
  Définit des langages d'arbres pour représenter des propriétés sur les réseaux
  (formules du type « a = 1 ET b > 0 OU ... »)
  et un langage impératif simple (incrémentation, conditionnelle, boucles, et quantificateurs)
  dans l'objectif de calculer une plus faible précondition à partir d'une postcondition
  et d'un programme donnés, sur un réseau donné.
  La plus faible précondition peut aussi être exportée en Answer Set Programming (ASP)
  pour être résolue avec Clingo 3.

Utilisation :
  Le modèle doit être modifié « en dur » dans la section « Informations du BRN ».
  Les résultats (calcul de plus faible précondition, simplifications, etc.) sont à écrire
  à la fin du programme dans les sections « Bac à sable ».
  Un exemple est déjà implémenté (quelques applications tirés du papier de référence).
  Le programme est fonctionnel et peut être exécuté par la commande :
    ocaml main.ml

Limitations théoriques :
  * Il faut pour le moment définir un invariant explicite pour les boulces (Tant que)
  * Les pré-conditions de boucles utilisent des sous-formules à part
      (avec des variables « fraiches ») qui ne sont pas impactées par la simplification
Limitations techniques :
  * Le modèle et les traitements doivent être implémentés « en dur » dans le code
  * La résolution par Clingo peut être très longue pour certaines formules
  * La sortie de Clingo peut être difficile à lire

Fonctions utiles :
  - wp : calcule la plus faible précondition associée à une postcondition et un programme donnés
  - simplify : simplifie une formule de type précondition ou postcondition, grâce
        à des propriétés logiques simples comme les règles de De Morgan
  - string_of_formula : traduit une formule en chaîne de caractères affichable
  - string_of_prog_indent : idem pour un programma impératif (avec indentations)
  - write_example : traduit une formule en ASP et l'écrit dans un fichier
  - asp_params : affiche la correspondance entre les variables ASP et les paramètres du modèle

TODO :

  * « Purifier » les fonctions
  * Faire un calcul automatique de plus faible invariant pour les boulces
  * Terminer les traitements des FreshState (simplifications, etc.)
  * Passer à Clingo 5
***)



(****************************)
(*** Fonction utilitaires ***)
(****************************)

open List ;;

(** Fonction identité **)
let id x = x ;;

(** Conversion d'une liste en chaîne de caractères selon une fonction de transformation et un séparateur *)
let rec string_of_list conv sep l =
  match l with
    | [] -> ""
    | h :: [] -> (conv h)
    | h :: t -> ((conv h) ^ sep ^ (string_of_list conv sep t)) ;;

let string_of_list_delim conv sep delim1 delim2 l =
  delim1 ^ string_of_list conv sep l ^ delim2 ;;

(** Calcul de l'ensemble des parties *)
let powerset l =
  let rec ps lp l =
    match l with
      | [] -> [lp]
      | h :: t -> (ps lp t) @ (ps (lp @ [h]) t) in
  ps [] l ;;

(** Calcul de l'ensemble des parties ordonné *)
let powerset_sort l fcompare =
  let rec ps lp l fcompare =
    match l with
      | [] -> [sort fcompare lp]
      | h :: t -> (ps lp t fcompare) @ (ps (lp @ [h]) t fcompare) in
  ps [] l fcompare ;;

(** Récupération de l'index d'un élément d'une liste *)
let index l a =
  let rec index_rec l a n =
    match l with
      | [] -> raise Not_found
      | h :: t -> if h = a then n else index_rec t a (n+1) in
  index_rec l a 0 ;;



(**************************)
(*** Opérateurs communs ***)
(**************************)

(** Opérateurs logiques *)
type propop2 =
  | And | Or | Impl ;;
type propop1 =
  | Neg ;;
type propop0 =
  | True | False ;;

(** Opérateurs logiques et arithmétiques *)
type relop2 =
  | LE | LT | GE | GT | Eq | NEq ;;
type exprop2 =
  | Plus | Minus ;;

(** Conversion des opérateurs en chaînes de caractères pour ASP *)
let asp_of_relop2 = function
  | LE -> "<="
  | LT -> "<"
  | GE -> ">="
  | GT -> ">"
  | Eq -> "=="
  | NEq -> "!=" ;;
let asp_of_exprop2 = function
  | Plus -> "+"
  | Minus -> "-" ;;

(** Conversion des opérateurs en chaînes de caractères pour affichage *)
let string_of_propop2 = function
  | And -> "&"
  | Or -> "|"
  | Impl -> "->" ;;
let string_of_propop1 = function
  | Neg -> "!" ;;
let string_of_propop0 = function
  | True -> "true"
  | False -> "false" ;;
let string_of_relop2 = function
  | LE -> "<="
  | LT -> "<"
  | GE -> ">="
  | GT -> ">"
  | Eq -> "="
  | NEq -> "!=" ;;
let string_of_exprop2 = function
  | Plus -> "+"
  | Minus -> "−" ;;



(**********************)
(*** Types d'un BRN ***)
(**********************)

(** Types **)
type var = string ;;
type mult = string ;;
type varmax = int ;;
type varpredec = mult list ;;

(** Fonctions de comparaison **)
let varequals = (=) ;;
let multcompare = compare ;;



(**************************************************)
(*** Grammaire pour les formules de multiplexes ***)
(**************************************************)

(** Expression et formules de multiplexes *)
type multexpr =
  | MExprBin of exprop2 * multexpr * multexpr
  | MExprConst of int ;;
type multformula =
  | MPropConst of propop0
  | MPropUn of propop1 * multformula
  | MPropBin of propop2 * multformula * multformula
  | MRel of relop2 * multexpr * multexpr
  | MAtom of var * int
  | MMult of mult ;;

