-------------------------------------------------------------
This directory contains experiments presented in the paper

"Greening R.Thomas' framework with Environement Variables:
 a Divide and Conquer approach"

by Laetitia Gibart   Hélène Collavizza   Jean-Paul Comet

To be published in the proceeding of CMSB21
-------------------------------------------------------------

This directory contains :
- this readme file and 
- Two directories:
  . Pseudomonas_model
  . Metabolism_model

A short TotemBioNet manual is available at the following URL:
https://gitlab.com/totembionet/totembionet/-/blob/master/doc/userManual.pdf


How to run the examples
-----------------------
In each directory there are two subdirectories:
- All in All (global approach to simulate all environments) 
- Env_by_Env (the divide and conquer approach).

In each directory, the file <x_experiment>.smb contains the input file
for TotemBioNet and <x_experiment>.csv are the output files.

To run the example again, you can run the command:
>>> totembionet -csv  -verbose 2 <path_to_example>/<example>.smb


1- Directories pseudomonas_model/All_in_All :
---------------------------------------------
- This directory refers to the  section 3 on the paper.
  It corresponds to the global approach to handle environments with
  R. Thomas' with artefacts approach.
- It contains 1 input (.smb) files, 1 output file in csv (.csv) 

2- Directories pseudomonas_model/Env_by_env :
---------------------------------------------
- This directory refers to the section 4 on the paper.
  It corresponds to "the divide an conquer" approach proceeding
  environment by environment. 
- It contains 2 input files (.smb) and 3 output files in csv (.csv)
- Each input file name refers to the biological context given by
  environment variable Calcium.  
- The directory also contains intersectionOK.csv. It lists all the
  models of the list of environmental properties.
  To obtain this output you need to use intersection option with an
  input directory (containing all the results of each environmental
  property):
  >>> totembionet -intersection -verbose 2 <path to example>/pseudomonas_model/Env_by_env/

3- Directory Metabolism_model/All_in_All :
------------------------------------------
- This directory refers to the  section 6.2 on the paper.
  It corresponds to the global approach to handle environments with
  R. Thomas' with artefacts approach for the metabolism model example.
- It contains 1 input file (.smb)
  (the output file corresponding to the global approach for the
  influence graph of metabolism regulation is not available because of
  the time mandatory to compute it, see article)

4- Directories Metabolism_model/Env_by_env :
--------------------------------------------
- This directory refers to the section 6.3 on the paper.
  It corresponds to "the divide an conquer" approach proceeding
  environment by environment. 
- It contains 36 input files (.smb) and 37 output files (.csv).    
- Each input name refers to the biological context numbering in Table
  1 and 2 of Appendices. Associated output file is named accordingly.  
- The directory also contains intersectionOK.csv. It lists all the
  models of the list of environment properties.
  To obtain this ouput you need to use intersection option with an
  input directory (containing all the results of each environmental
  property):
  >>> totembionet -intersection -verbose 2 <path to example>/Metabolism_model/Env_by_env/


 
