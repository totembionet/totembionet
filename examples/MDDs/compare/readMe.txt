Examples of comparisons using MDDs
----------------------------------

Option -compare path/f1.csv path/f2.csv to compare 2 csv files which are in TotemBioNet format (i.e. first line contains variables, second line their domains, first column number of model, second column OK or KO).
This builds the MDD : MDD_f1 and not MDD_f2. If this MDD is empty, files contain the same values, otherwise files differ, and the difference is written in two .csv : f1-noInt-f2.csv and f2-notInf1.csv.

Boxes of f1 and f2 can contain values "min..max" which means "any value between min and max" or "-" which means any value in the domain of the variable.

In this directory:
- f1-copy is a copy of f1. The comparison returns "equal".
- f1 and f2 differs, they both contain values which are not in the other file
- simple has variables which are not in f1 thus the comparison returns "not equal"
- f3 is a copy paste of f1 where "don't car values have been instanciated.
