Files are copies of f1 in which :
  * some different OK models have been removed in the different files except the first line which remain in all the files
  * the last KO line of f1 is everywhere 

The result should be the first line for OK models (in intersectionOK.csv) and 16 KO model (in intersectionKO.csv). Intersection of KO models are obtained running TotemBioNet with options -intersection -KO.


