-------------------------------------------------------------
This directory contains experiments presented in the paper

"Regulation of eukaryote metabolism: an abstract model"

by Laetitia Gibart, Khooderam Rajeev, Gilles Bernot,  
   Jean-Paul Comet and Jean-Yves Trosset

Submitted in the special issue of the journal 
"Frontiers in Connecting Steady-State and Dynamic Approaches 
 for Modelling Cell Metabolic Behavior"

-------------------------------------------------------------
This directory contains :
- this readme file,
- intersectionOK.csv file
- and two directories:
  . smbfiles
  . csvfiles

The file "intersectionOK.csv" is the final output of TotemBioNet 
that indicates how many parametrisations of the regulatory network 
are consistant with given global behaviour traduced in CTL 
formulas (validation matrix). 

A short TotemBioNet manual is available at the following URL:
https://gitlab.com/totembionet/totembionet/-/blob/master/doc/userManual.pdf


How to run the examples
-----------------------

First each input file in smbfile repertory must be processed.

Files <x_experiment>.smb are input files for TotemBioNet and <x_experiment>.csv are the associated output files.

To run TotemBioNet on an example file, the correct command is: 
>>> totembionet -csv  -verbose 2 <path_to_example>/<example>.smb

Each smb file concerns an environment. To have the set of parametrisations that satisfy all the 36 environment specifications, one hase to compute the "intersection" between the results obtained previously. 

This last step is computed using the command:
>>> totembionet -intersection <path_to_csvfile_directory>

The results are memorized in the csv output file. 



